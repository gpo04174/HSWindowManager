﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;

namespace HSWindowManager.Spy
{
    public partial class SpyControl: UserControl
    {
        public SpyControl()
        {
            InitializeComponent();

            SetStyle(ControlStyles.FixedWidth, true);
            SetStyle(ControlStyles.FixedHeight, true);
            SetStyle(ControlStyles.StandardClick, false);
            SetStyle(ControlStyles.StandardDoubleClick, false);
            SetStyle(ControlStyles.Selectable, false);

            try
            {
                // Load cursors
                Assembly assembly = Assembly.GetExecutingAssembly();
                //assembly.GetManifestResourceStream("WindowFinder.curTarget.cur")
                //new Bitmap(assembly.GetManifestResourceStream("WindowFinder.bmpFind.bmp"))
                //new Bitmap(assembly.GetManifestResourceStream("WindowFinder.bmpFinda.bmp"))
                MemoryStream cursor = new MemoryStream(global::HSWindowManager.Properties.Resources.curTarget);
                this.cursor = new Cursor(cursor);
                bitmapFind = global::HSWindowManager.Properties.Resources.bmpFind;
                bitmapFinda = global::HSWindowManager.Properties.Resources.bmpFinda;
                //SetTransparent(Highlighter.Handle, true);
            }
            catch (Exception x)
            {
                // Show error
                MessageBox.Show(this, "리소스를 로드할 수 없습니다.\n\n" + x.ToString(), "창 찾기");

                // Attempt to use backup cursor
                if (cursor == null)cursor = Cursors.Cross;
            }
        }

        void Init(bool TopMost)
        {
            if (Highlighter == null || Highlighter.IsDisposed) Highlighter = new Form()
            {
                TopMost = TopMost,
                ShowInTaskbar = false,
                StartPosition = System.Windows.Forms.FormStartPosition.Manual,
                BackColor = System.Drawing.Color.Fuchsia,
                TransparencyKey = System.Drawing.Color.Fuchsia,
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
            };
        }

        #region Private
        IntPtr parent;
        IntPtr curWin;

        Cursor cursor;
        Bitmap bitmapFind;
        Bitmap bitmapFinda;

        Form Highlighter;

        bool running;
        #endregion

        #region Public Properties

        public event GrabbedEventHandler Grabbed;
        public event GrabbedEventHandler GrabFinished;
        public event EventHandler GrabLosted;

        public IntPtr LastHandle { get; private set; }

        bool _TopMost = true;
        public bool TopMost
        {
            get { return Highlighter == null ? _TopMost : Highlighter.TopMost; }
            set { _TopMost = TopMost; if (Highlighter != null && !Highlighter.IsDisposed) Highlighter.TopMost = value; }
        }

        #endregion

        #region Event Handler Methods
        private void SpyControl_MouseUp(object sender, MouseEventArgs e)
        {
            Highlighter.Hide();
            if (e.Button == MouseButtons.Left)
            {
                try
                {
                    if (curWin == IntPtr.Zero) { GrabLosted?.Invoke(this, null); }
                    else GrabFinished?.Invoke(this, LastHandle);
                }
                catch { }
            }

            if (this.ParentForm != null) this.ParentForm.Opacity = 1;
            LastHandle = IntPtr.Zero;
            //SetTransparent(this.Handle, false);
            this.Cursor = Cursors.Arrow;
            running = false;
            //Highlighter.Refresh();
            this.BackgroundImage = Properties.Resources.bmpFind;
            //Highlighter.Location = new Point(5000, 5000);
        }

        private void SpyControl_MouseMove(object sender, MouseEventArgs e)
        {
            POINT pt = e.Location;
            ClientToScreen(this.Handle, ref pt);

            curWin = WindowFromPoint(pt);
            if (curWin != LastHandle && running)
            {
                LastHandle = curWin;
                try { Grabbed?.Invoke(this, LastHandle); } catch { }

                RECT rect = new RECT();
                GetWindowRect(curWin, out rect);
                Console.WriteLine((Rectangle)rect);
                Highlighter.Bounds = rect;
                Highlighter.Refresh();
            }
        }

        private void SpyControl_MouseDown(object sender, MouseEventArgs e)
        {
            Init(TopMost);
            Highlighter.Paint += Highlight_OnPaint;
            Highlighter.Show();
            Highlighter.BringToFront();

            this.BackgroundImage = bitmapFinda;
            parent = this.Parent.Handle;
            running = true;
            this.Cursor = cursor;
            //SetTransparent(this.Parent.Handle, true);
            this.ParentForm.Opacity = 0.9;
        }

        private void SpyControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                SpyControl_MouseUp(sender, new MouseEventArgs(MouseButtons.None, 0, 0, 0, 0));
        }

        private void SpyControl_ParentChanged(object sender, EventArgs e)
        {
            if(Highlighter != null && !Highlighter.IsDisposed) Highlighter.Show((IWin32Window)sender);
        }
        
        private void Highlight_OnPaint(object sender, PaintEventArgs e)
        {
            //Console.WriteLine(e.ClipRectangle.ToString());
            if (running)
            {
                Rectangle rect = e.ClipRectangle;
                if (this.Left < 0 && this.Top < 0 && this.Height > Screen.PrimaryScreen.WorkingArea.Height && this.Width > Screen.PrimaryScreen.WorkingArea.Width)
                {
                    rect.Size = Screen.PrimaryScreen.WorkingArea.Size;
                    rect.X = Screen.PrimaryScreen.WorkingArea.Left - this.Left;
                    rect.Y = Screen.PrimaryScreen.WorkingArea.Top - this.Top;
                }

                rect.X++;
                rect.Y++;
                rect.Width -= 2;
                rect.Height -= 2;
                e.Graphics.DrawRectangle(new Pen(Color.Red, 2), rect);
            }
        }
        #endregion

        #region WinAPI
        #region WinAPI Method   
        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        static extern int GetWindowLong32(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr")]
        static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);
        static IntPtr GetWindowLong(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size > 4)
                return GetWindowLongPtr64(hWnd, nIndex);
            else
                return (IntPtr)GetWindowLong32(hWnd, nIndex);
        }
        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        static extern int SetWindowLong32(IntPtr hWnd, int nIndex, int dwNewLong);
        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        static extern IntPtr SetWindowLongPtr64(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
        static IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size > 4)
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            else
                return (IntPtr)SetWindowLong32(hWnd, nIndex, (int)dwNewLong);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

        [DllImport("user32.dll")]
        static extern IntPtr WindowFromPoint(POINT point);
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        static extern bool ClientToScreen(IntPtr hWnd, ref POINT pt);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        #endregion

        #region WinAPI Enum
        public enum WindowExStyles : int
        {
            WS_EX_DLGMODALFRAME = 0x00000001,
            WS_EX_NOPARENTNOTIFY = 0x00000004,
            WS_EX_TOPMOST = 0x00000008,
            WS_EX_ACCEPTFILES = 0x00000010,
            /// <summary>
            /// Specifies that a window should not be painted until siblings beneath the window (that were created by the same thread) 
            /// have been painted. You can use it for hit testing - the mouse events will be passed to the other windows underneath the layered window.
            /// </summary>
            WS_EX_TRANSPARENT = 0x00000020,
            WS_EX_MDICHILD = 0x00000040,
            WS_EX_TOOLWINDOW = 0x00000080,
            WS_EX_WINDOWEDGE = 0x00000100,
            WS_EX_CLIENTEDGE = 0x00000200,
            WS_EX_CONTEXTHELP = 0x00000400,
            WS_EX_RIGHT = 0x00001000,
            WS_EX_LEFT = 0x00000000,
            WS_EX_RTLREADING = 0x00002000,
            WS_EX_LTRREADING = 0x00000000,
            WS_EX_LEFTSCROLLBAR = 0x00004000,
            WS_EX_RIGHTSCROLLBAR = 0x00000000,
            WS_EX_CONTROLPARENT = 0x00010000,
            WS_EX_STATICEDGE = 0x00020000,
            WS_EX_APPWINDOW = 0x00040000,
            WS_EX_OVERLAPPEDWINDOW = (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE),
            WS_EX_PALETTEWINDOW = (WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST),
            WS_EX_LAYERED = 0x00080000,
            WS_EX_NOINHERITLAYOUT = 0x00100000, // Disable inheritence of mirroring by children
            WS_EX_LAYOUTRTL = 0x00400000, // Right to left mirroring
            WS_EX_COMPOSITED = 0x02000000,
            WS_EX_NOACTIVATE = 0x08000000
        }
        #endregion

        #region WinAPI Struct
        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int x;
            public int y;

            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public static implicit operator Point(POINT p)
            {
                return new Point(p.x, p.y);
            }

            public static implicit operator POINT(Point p)
            {
                return new POINT(p.X, p.Y);
            }
        }


        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;

            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }
            public RECT(Rectangle r)
            {
                this.left = r.Left;
                this.top = r.Top;
                this.right = r.Right;
                this.bottom = r.Bottom;
            }

            public static RECT Empty { get { return new RECT(0, 0, 0, 0); } }
            public int Height { get { return bottom - top; } }
            public int Width { get { return right - left; } }
            public Size Size { get { return new Size(this.right - this.left, this.bottom - this.top); } }
            public Point Location { get { return new Point(left, top); } }

            public static implicit operator RECT(Rectangle p) { return new RECT(p); }
            public static implicit operator Rectangle(RECT p) { return Rectangle.FromLTRB(p.left, p.top, p.right, p.bottom); }

            public static RECT FromXYWH(int x, int y, int width, int height) { return new RECT(x, y, x + width, y + height); }
        }
        #endregion
        #endregion

        #region Helper Methods

        /// <summary>
        /// Highlights the specified window, but only if it is a valid window in relation to the specified owner window.
        /// </summary>
        public void HighlightWindow() { HighlightWindow(LastHandle); }
        public void HighlightWindow(IntPtr Handle)
        {
            RECT rect = new RECT();
            GetWindowRect(Handle, out rect);
            if (rect.Width > 0 && rect.Height > 0)
            {
                Init(TopMost);
                Highlighter.Show(this);
                Application.DoEvents();
                for (int i = 0; i < 5; i++)
                {
                    Highlighter.Bounds = rect;
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                    Highlighter.Hide();
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(100);
                    Highlighter.Show(this);
                    Highlighter.Refresh();
                }
                Highlighter.Hide();
                Application.DoEvents();
            }
        }

        private void SetTransparent(IntPtr Handle, bool IsTransparent)
        {
            const int GWL_EXSTYLE = -20;
            WindowExStyles style = (WindowExStyles)GetWindowLong(Handle, GWL_EXSTYLE);
            SetWindowLong(Handle, GWL_EXSTYLE, IsTransparent ? (IntPtr)(style | WindowExStyles.WS_EX_TRANSPARENT) : (IntPtr)(style |~ WindowExStyles.WS_EX_TRANSPARENT));

            ///SetWindowPos(Handle, 0, 0, 0, 0, 0, WinPosFlags.SWP_FRAMECHANGED | WinPosFlags.SWP_NOZORDER | WinPosFlags.SWP_NOSIZE | WinPosFlags.SWP_NOMOVE | WinPosFlags.SWP_NOACTIVATE | WinPosFlags.SWP_NOOWNERZORDER);
        }

        #endregion

        /*
        public new Color BackColor
        {
            get { return pictureBox1.BackColor; }
            set { pictureBox1.BackColor = value; }
        }*/

        #region Design
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SpyControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::HSWindowManager.Properties.Resources.bmpFind;
            this.MaximumSize = new System.Drawing.Size(31, 28);
            this.MinimumSize = new System.Drawing.Size(31, 28);
            this.Name = "SpyControl";
            this.Size = new System.Drawing.Size(31, 28);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SpyControl_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SpyControl_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SpyControl_MouseUp);
            this.ParentChanged += new System.EventHandler(this.SpyControl_ParentChanged);
            this.KeyDown += SpyControl_KeyDown;
            this.ResumeLayout(false);
        }
        #endregion
    }
}
