﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Text;
using System.Windows.Forms;

namespace HSWindowManager
{
    [Flags]
    public enum BOOL : int {True = 0, False = 1, Unknown = -1, Denied = 5 }
    public class WindowItem
    {
        WindowItemEx cw;
        Process _Process;

        internal WindowItem() { }
        internal WindowItem(Process Process)  {this._Process = Process; }
        internal WindowItem(WindowItemEx windowex)
        {
            this.WindowItemEx = windowex;
            this.WindowHandle = windowex.Handle;

            if (!WindowItemEx.Exists /*style == 0*/) throw new Exception("올바르지 않는 창 핸들 입니다.");

            CurrentProcess = Process.GetProcessById((int)WindowItemEx.ProcessId);
            if (CurrentProcess != null)
                for (int i = 0; i < CurrentProcess.Threads.Count; i++)
                    if (CurrentProcess.Threads[i].Id == WindowItemEx.ThreadId)
                    { CurrentThread = CurrentProcess.Threads[i]; break; }
        }
        public WindowItem(IntPtr WindowHandle)
        {
            this.WindowHandle = WindowHandle;
            cw = WindowHandle;
            //int style = WindowAPI.GetWindowLong(WindowHandle, WindowAPI.GWL_EXSTYLE);
            if (!cw.Exists /*style == 0*/) throw new Exception("올바르지 않는 창 핸들 입니다.");

            CurrentProcess = Process.GetProcessById((int)WindowItemEx.ProcessId);
            if (CurrentProcess != null)
                for (int i = 0; i < CurrentProcess.Threads.Count; i++)
                    if (CurrentProcess.Threads[i].Id == WindowItemEx.ThreadId)
                    { CurrentThread = CurrentProcess.Threads[i]; break; }
        }
        public WindowItem(IntPtr WindowHandle, Process Process)
        {
            this.WindowHandle = WindowHandle;
            cw = WindowHandle;
            //int style = WindowAPI.GetWindowLong(WindowHandle, WindowAPI.GWL_EXSTYLE);
            if (!cw.Exists /*style == 0*/) throw new Exception("올바르지 않는 창 핸들 입니다.");
            CurrentProcess = Process;
            if (CurrentProcess != null)
                for (int i = 0; i < CurrentProcess.Threads.Count; i++)
                    if (CurrentProcess.Threads[i].Id == WindowItemEx.ThreadId)
                    { CurrentThread = CurrentProcess.Threads[i]; break; }
        }

        public Process Process
        {
            get
            {
                if (_Process == null)
                {
                    int processId;
                    WindowAPI.GetWindowThreadProcessId(WindowHandle, out processId);
                    return Process.GetProcessById(processId);
                }
                else return _Process;
            }
            set { Process = value; }
        }
        public IntPtr WindowHandle { get; internal set; }

        public bool LastSuccess { get; private set; }

        public WindowItemEx WindowItemEx { get { return cw; } private set { cw = value; } }
        /// <summary>
        /// Determines whether the specified window is a native Unicode window. For more information, see IsWindowUnicode in MSDN.
        /// </summary>
        //public bool IsUnicodeWindow{get { return IsWindowUnicode(this.WindowHandle); } }

        public string Class
        {
            get
            {
                //int cls_max_length = 1000;
                //StringBuilder classText = new StringBuilder("", cls_max_length + 5);
                //WindowAPI.GetClassName(WindowHandle, classText, cls_max_length + 2);
                return WindowItemEx.ClassName + (WindowItemEx.RealClassName == WindowItemEx.ClassName ? "" : " (" + WindowItemEx.RealClassName + ")");
            }
        }

        public string Title
        {
            get
            {
                int length = WindowAPI.GetWindowTextLength(WindowHandle) * 2;
                StringBuilder sbWinText = new StringBuilder(length);
                LastSuccess = WindowAPI.GetWindowText(WindowHandle, sbWinText, length);
                return sbWinText.ToString();
            }
            set
            {
                if (!cw.IsHung) cw.TextUnsafe = value;
                else LastSuccess = WindowAPI.SetWindowText(WindowHandle, value);
            }
        }
        public byte Transparent
        {
            get
            {
                uint crKey, dwFlag;
                byte bAlpha;
                LastSuccess = WindowAPI.GetLayeredWindowAttributes(WindowHandle, out crKey, out bAlpha, out dwFlag);
                return dwFlag == 0 && bAlpha == 0 ? (byte)255 : bAlpha;
            }
            set
            {
                byte val = Math.Max(Math.Min(value, (byte)255), (byte)1);
                int style = WindowAPI.GetWindowLong(WindowHandle, WindowAPI.GWL_EXSTYLE);
                WindowAPI.SetWindowLong(WindowHandle, WindowAPI.GWL_EXSTYLE, style | WindowAPI.WS_EX_LAYERED);
                LastSuccess = WindowAPI.SetLayeredWindowAttributes(WindowHandle, 0, val, WindowAPI.LWA_ALPHA);
            }
        }
        public bool AlwaysOnTop
        {
            get { return WindowItemEx.TopMost; }
            set{LastSuccess = WindowAPI.SetWindowPos(WindowHandle, value ? WindowAPI.HWND_TOPMOST : WindowAPI.HWND_NOTOPMOST, 0, 0, 0, 0, WindowAPI.TOPMOST_FLAGS);}
        }
        public bool Enable
        {
            get { return WindowAPI.IsWindowEnabled(WindowHandle); }
            set{ LastSuccess = WindowAPI.EnableWindow(WindowHandle, value) == 0;}
        }
        public bool Visible
        {
            get{return WindowAPI.IsWindowVisible(WindowHandle) == 1;}
            set{LastSuccess = WindowAPI.ShowWindow(WindowHandle, value ? WindowAPI.SW_SHOW : WindowAPI.SW_HIDE);}
        }
        public BOOL CloseButton
        {
            get
            {
                IntPtr sm = WindowAPI.GetSystemMenu(WindowHandle, false);
                uint state = WindowAPI.GetMenuState(sm, WindowAPI.SC_CLOSE, WindowAPI.MF_BYCOMMAND);
                if (state == 536875008) return BOOL.True;
                else if (state == 0xFFFFFFFF) return BOOL.Unknown;
                else return BOOL.False;
            }
            set
            {
                IntPtr sm = WindowAPI.GetSystemMenu(WindowHandle, false);
                //uint state = WindowAPI.GetMenuState(sm, WindowAPI.SC_CLOSE, WindowAPI.Enable.MF_BYCOMMAND);
                LastSuccess = WindowAPI.EnableMenuItem(sm, WindowAPI.SC_CLOSE, WindowAPI.MF_BYCOMMAND | (CloseButton == BOOL.True ? WindowAPI.MF_DISABLED : WindowAPI.MF_ENABLED));
            }
        }

        public Process CurrentProcess { get; private set; }
        public ProcessThread CurrentThread { get; private set; }
        public BOOL IsPauseThread
        {
            get
            {
                Process pr = Process.GetProcessById((int)WindowItemEx.ProcessId);
                if (pr != null)
                    for (int i = 0; i < pr.Threads.Count; i++)
                        if (pr.Threads[i].Id == CurrentThread.Id)
                            return 
                                pr.Threads[i].ThreadState == ThreadState.Wait && 
                                pr.Threads[i].WaitReason == ThreadWaitReason.Suspended ? BOOL.True : BOOL.False; 
                return BOOL.Unknown;
            }
        }

        /*
        public FormBorderStyle WindowBorderStyle
        public FormWindowState WindowState{ get; private set; }
        public BorderStyle WindowStyle { get; set; }
        [Obsolete]
        public bool MouseEnable { get; set; }
        */

        public int Blink() { return WindowAPI.FlashWindow(WindowHandle, true); }


        public int BringToFront() {return WindowAPI.BringWindowToTop(WindowHandle); }
        public int Close() { return WindowAPI.SendMessage(WindowHandle, WindowAPI.WM_SYSCOMMAND, WindowAPI.SC_CLOSE, 0); }
        public bool Restore() { return WindowAPI.ShowWindow(WindowHandle, WindowAPI.SW_SHOWNORMAL); }
        public bool Maximize(){ return WindowAPI.ShowWindow(WindowHandle, WindowAPI.SW_SHOWMAXIMIZED);}
        public bool Minimize() { return WindowAPI.ShowWindow(WindowHandle, WindowAPI.SW_SHOWMINIMIZED);}


        public static implicit operator WindowItem(WindowItemEx windowex) { return new WindowItem(windowex); }
        public static implicit operator WindowItem(IntPtr Handle) { return new WindowItem(Handle); }
    }
    

    static class WindowAPI
    {
        public const int GWL_EXSTYLE = -20;
        public const int GWL_STYLE = -16;
        public enum WindowStyle
        {
            WS_OVERLAPPED = 0x00000000,
            WS_POPUP = -2147483648, //0x80000000,
            WS_CHILD = 0x40000000,
            WS_MINIMIZE = 0x20000000,
            WS_VISIBLE = 0x10000000,
            WS_DISABLED = 0x08000000,
            WS_CLIPSIBLINGS = 0x04000000,
            WS_CLIPCHILDREN = 0x02000000,
            WS_MAXIMIZE = 0x01000000,
            WS_CAPTION = 0x00C00000,
            WS_BORDER = 0x00800000,
            WS_DLGFRAME = 0x00400000,
            WS_VSCROLL = 0x00200000,
            WS_HSCROLL = 0x00100000,
            WS_SYSMENU = 0x00080000,
            WS_THICKFRAME = 0x00040000,
            WS_GROUP = 0x00020000,
            WS_TABSTOP = 0x00010000,
            WS_MINIMIZEBOX = 0x00020000,
            WS_MAXIMIZEBOX = 0x00010000,
            WS_TILED = WS_OVERLAPPED,
            WS_ICONIC = WS_MINIMIZE,
            WS_SIZEBOX = WS_THICKFRAME,
            WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW,
            WS_OVERLAPPEDWINDOW = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX),
            WS_POPUPWINDOW = (WS_POPUP | WS_BORDER | WS_SYSMENU),
            WS_CHILDWINDOW = (WS_CHILD)
        }


        public const int MF_BYCOMMAND = 0x00000000;
        public const int MF_BYPOSITION = 0x00000400;
        public const int MF_DISABLED = 0x00000002;
        public const int MF_ENABLED = 0x00000000;
        public const int MF_GRAYED = 0x00000001;

        public const int WS_EX_LAYERED = 0x00080000;

        public const int LWA_ALPHA = 0x00000002;

        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_CLOSE = 0xF060;

        public const int SW_HIDE = 0;
        public const int SW_SHOWNORMAL = 1;
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_SHOWMAXIMIZED = 3;
        public const int SW_SHOW = 5;

        public const int HWND_TOPMOST = -1;
        public const int HWND_NOTOPMOST = -2;
        private const int SWP_NOSIZE = 0x0001;
        private const int SWP_NOMOVE = 0x0002;
        public const int SWP_HIDEWINDOW = 0x0080;
        public const int SWP_SHOWWINDOW = 0x0040;
        public const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern long GetClassName(IntPtr hwnd, StringBuilder lpClassName, long nMaxCount);

        [DllImport("user32")]
        public static extern Int32 SetWindowLong(IntPtr hWnd, Int32 nIndex, Int32 dwNewLong);
        [DllImport("user32")]
        public static extern Int32 GetWindowLong(IntPtr hWnd, Int32 nIndex);

        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        [DllImport("User32.dll")]
        public static extern Int32 SetForegroundWindow(IntPtr hWnd);
        [DllImport("User32.dll")]
        public static extern Int32 BringWindowToTop(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetLayeredWindowAttributes(IntPtr hwnd, out uint crKey, out byte bAlpha, out uint dwFlags);

        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32")]
        public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);

        [DllImport("user32")]
        public static extern int IsWindowVisible(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern int FlashWindow(IntPtr hWnd, bool bInvert);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern bool GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern bool SetWindowText(IntPtr hWnd, String lpString);

        [DllImport("user32.dll")]
        public static extern bool IsWindowUnicode(IntPtr hWnd);

        [DllImport("user32")]
        public static extern int EnableWindow(IntPtr hWnd, bool bEnable);
        [DllImport("user32")]
        public static extern bool IsWindowEnabled(IntPtr hWnd);

        [DllImport("user32", SetLastError = true)]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int Pid);

        [DllImport("user32")]
        public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32")]
        public static extern bool EnableMenuItem(IntPtr hMenu, int uIDEnableItem, int uEnable);
        
        [DllImport("user32.dll")]
        public static extern uint GetMenuState(IntPtr hmenu, int nID, int nFlags);
    }
    static class WindowAPIEx
    {
        public const Int32 LVM_FIRST = 0x1000;
        public const Int32 LVM_SCROLL = LVM_FIRST + 20;
        public const int SBS_HORZ = 0;
        public const int SBS_VERT = 1;

        [DllImport("user32.dll")]
        public static extern int GetScrollPos(System.IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern bool LockWindowUpdate(IntPtr Handle);
    }

    static class KernelAPI
    {
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool CloseHandle(IntPtr handle);

        #region Thread
        #region Helper
        public static BOOL ThreadSuspend(int ThreadID, bool IsWow64)
        {
            int result = (int)BOOL.Unknown;
            IntPtr pOpenThread = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)ThreadID);
            IntPtr pOpenThread1 = OpenThread(ThreadAccess.QUERY_INFORMATION, false, (uint)ThreadID);

            if (pOpenThread == IntPtr.Zero || pOpenThread1 == IntPtr.Zero) result = (int)BOOL.Unknown;
            else if (pOpenThread == IntPtr.Zero && pOpenThread1 != IntPtr.Zero) result = (int)BOOL.Denied;
            else
            {
                if (IsWow64) result = SuspendThread64(pOpenThread);
                else result = SuspendThread32(pOpenThread);
            }

            CloseHandle(pOpenThread);
            CloseHandle(pOpenThread1);

            if (result == 1) return BOOL.True;
            else if (result == 0) return BOOL.False;
            else return (BOOL)result;
        }
        public static BOOL ThreadResume(int ThreadID)
        {
            int result = (int)BOOL.Unknown;
            IntPtr pOpenThread = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)ThreadID);
            IntPtr pOpenThread1 = OpenThread(ThreadAccess.QUERY_INFORMATION, false, (uint)ThreadID);

            if (pOpenThread == IntPtr.Zero || pOpenThread1 == IntPtr.Zero) result = (int)BOOL.Unknown;
            else if (pOpenThread == IntPtr.Zero && pOpenThread1 != IntPtr.Zero) result = (int)BOOL.Denied;
            else result = ResumeThread(pOpenThread);

            CloseHandle(pOpenThread);
            CloseHandle(pOpenThread1);

            if (result == 1) return BOOL.True;
            else if (result == 0) return BOOL.False;
            else return (BOOL)result;
        }
        public static BOOL ThreadTerminate(int ThreadID, int ExitCode = 0)
        {
            int result = (int)BOOL.Unknown;
            IntPtr pOpenThread = OpenThread(ThreadAccess.TERMINATE, false, (uint)ThreadID);
            IntPtr pOpenThread1 = OpenThread(ThreadAccess.QUERY_INFORMATION, false, (uint)ThreadID);

            if (pOpenThread == IntPtr.Zero || pOpenThread1 == IntPtr.Zero) result = (int)BOOL.Unknown;
            else if (pOpenThread == IntPtr.Zero && pOpenThread1 != IntPtr.Zero) result = (int)BOOL.Denied;
            else result = TerminateThread(pOpenThread, ExitCode);

            CloseHandle(pOpenThread);
            CloseHandle(pOpenThread1);

            if (result == 1) return BOOL.True;
            else if (result == 0) return BOOL.False;
            else return (BOOL)result;
        }

        [System.Security.SecuritySafeCritical]
        public static bool ProcessIsWow64(Process process)
        {
            if ((Environment.OSVersion.Version.Major > 5) ||
                (Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1))
            {
                bool IsWow64;
                bool suc = IsWow64Process(process.Handle, out IsWow64);
                //return !(!suc || IsWow64);
                return IsWow64;
            }
            else return false;
        }

        #region Class
        #endregion

        #endregion

        #region Structs
        #endregion

        #region Enums
        [Flags]
        public enum ThreadAccess : int
        {
            TERMINATE = (0x0001),
            SUSPEND_RESUME = (0x0002),
            GET_CONTEXT = (0x0008),
            SET_CONTEXT = (0x0010),
            SET_INFORMATION = (0x0020),
            QUERY_INFORMATION = (0x0040),
            SET_THREAD_TOKEN = (0x0080),
            IMPERSONATE = (0x0100),
            DIRECT_IMPERSONATION = (0x0200)
        }
        #endregion

        #region Native Method
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

        [DllImport("Kernel32.dll", EntryPoint = "SuspendThread")]
        public static extern int SuspendThread32(IntPtr ThreadHandle);
        [DllImport("Kernel32.dll", EntryPoint = "Wow64SuspendThread")]
        public static extern int SuspendThread64(IntPtr ThreadHandle);

        [DllImport("Kernel32.dll", EntryPoint = "ResumeThread")]
        public static extern int ResumeThread(IntPtr ThreadHandle);

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern int TerminateThread(IntPtr ThreadHandle, int ExitCode);

        [DllImport("Kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        [ResourceExposure(ResourceScope.Machine)]
        public static extern bool IsWow64Process(IntPtr hSourceProcessHandle, out bool isWow64);

        [Obsolete]
        [DllImport("Kernel32.dll")]
        public static extern IntPtr GetProcessIdOfThread(IntPtr ThreadID);
        #endregion
        #endregion

    }

    static class ThemeAPI
    {
        [System.ComponentModel.Description("Windows Vista 이상에서 아이템에 투명 테마를 적용할지의 여부 입니다."),
        System.ComponentModel.DefaultValue(false),
        System.ComponentModel.Category("HSListView")]
        public static bool ApplyWindowTheme(IntPtr Handle, bool Apply, string Name = "explorer")
        {
            return SetWindowTheme(Handle, Apply ? "explorer" : null, null) != 0;
        }

        [DllImport("uxtheme", CharSet = CharSet.Unicode)]
        public static extern int SetWindowTheme(IntPtr hWnd, String subAppName, String subIdList);

        [DllImport("uxtheme")]
        public static extern IntPtr GetWindowTheme(IntPtr hWnd);

        [DllImport("uxtheme")]
        public static extern IntPtr OpenThemeData(IntPtr hWnd, string pszClassList);

    }
}
