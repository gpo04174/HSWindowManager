﻿namespace HSWindowManager
{
    partial class frmProcessList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침RToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.맨위로고정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chkProcessWindow = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.스레드목록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.일시중지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로세스재개ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.종료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.종료강제로ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.listView1 = new HSWindowManager.Utils.HSListViewEx();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(410, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새로고침RToolStripMenuItem,
            this.toolStripSeparator1,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 새로고침RToolStripMenuItem
            // 
            this.새로고침RToolStripMenuItem.Name = "새로고침RToolStripMenuItem";
            this.새로고침RToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.새로고침RToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.새로고침RToolStripMenuItem.Text = "새로고침(&R)";
            this.새로고침RToolStripMenuItem.Click += new System.EventHandler(this.새로고침RToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(153, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동새로고침ToolStripMenuItem,
            this.toolStripSeparator2,
            this.맨위로고정ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 자동새로고침ToolStripMenuItem
            // 
            this.자동새로고침ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.자동새로고침ToolStripMenuItem.Name = "자동새로고침ToolStripMenuItem";
            this.자동새로고침ToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.자동새로고침ToolStripMenuItem.Text = "자동 새로고침";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "멈춤",
            "느리게 (2초)",
            "보통 (1초)",
            "빠르게 (0.5초)",
            "아주 빠르게 (0.25초)"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(146, 6);
            // 
            // 맨위로고정ToolStripMenuItem
            // 
            this.맨위로고정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로고정ToolStripMenuItem.Name = "맨위로고정ToolStripMenuItem";
            this.맨위로고정ToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.맨위로고정ToolStripMenuItem.Text = "맨 위로 고정";
            // 
            // chkProcessWindow
            // 
            this.chkProcessWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkProcessWindow.AutoSize = true;
            this.chkProcessWindow.Checked = true;
            this.chkProcessWindow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProcessWindow.Location = new System.Drawing.Point(251, 4);
            this.chkProcessWindow.Name = "chkProcessWindow";
            this.chkProcessWindow.Size = new System.Drawing.Size(156, 16);
            this.chkProcessWindow.TabIndex = 4;
            this.chkProcessWindow.Text = "창 있는 프로세스만 표시";
            this.chkProcessWindow.UseVisualStyleBackColor = true;
            this.chkProcessWindow.CheckedChanged += new System.EventHandler(this.chkProcessWindow_CheckedChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.스레드목록ToolStripMenuItem,
            this.toolStripSeparator3,
            this.일시중지ToolStripMenuItem,
            this.프로세스재개ToolStripMenuItem,
            this.toolStripSeparator4,
            this.종료ToolStripMenuItem,
            this.종료강제로ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(233, 126);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 스레드목록ToolStripMenuItem
            // 
            this.스레드목록ToolStripMenuItem.Name = "스레드목록ToolStripMenuItem";
            this.스레드목록ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.스레드목록ToolStripMenuItem.Text = "스레드 목록";
            this.스레드목록ToolStripMenuItem.Click += new System.EventHandler(this.스레드목록ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(229, 6);
            // 
            // 일시중지ToolStripMenuItem
            // 
            this.일시중지ToolStripMenuItem.Name = "일시중지ToolStripMenuItem";
            this.일시중지ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.일시중지ToolStripMenuItem.Text = "프로세스 일시중지";
            this.일시중지ToolStripMenuItem.Click += new System.EventHandler(this.일시중지ToolStripMenuItem_Click);
            // 
            // 프로세스재개ToolStripMenuItem
            // 
            this.프로세스재개ToolStripMenuItem.Name = "프로세스재개ToolStripMenuItem";
            this.프로세스재개ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.프로세스재개ToolStripMenuItem.Text = "프로세스 재개";
            this.프로세스재개ToolStripMenuItem.Click += new System.EventHandler(this.프로세스재개ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(229, 6);
            // 
            // 종료ToolStripMenuItem
            // 
            this.종료ToolStripMenuItem.Name = "종료ToolStripMenuItem";
            this.종료ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.종료ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.종료ToolStripMenuItem.Text = "창 닫기";
            this.종료ToolStripMenuItem.Click += new System.EventHandler(this.종료ToolStripMenuItem_Click);
            // 
            // 종료강제로ToolStripMenuItem
            // 
            this.종료강제로ToolStripMenuItem.Name = "종료강제로ToolStripMenuItem";
            this.종료강제로ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.종료강제로ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.종료강제로ToolStripMenuItem.Text = "프로그램 종료 (강제로)";
            this.종료강제로ToolStripMenuItem.Click += new System.EventHandler(this.종료강제로ToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(279, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "목록 새로고침 (F5)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.새로고침RToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.새로고침RToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(97, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(179, 21);
            this.textBox1.TabIndex = 8;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "모두",
            "이름",
            "PID",
            "창 핸들",
            "창 타이틀"});
            this.comboBox1.Location = new System.Drawing.Point(2, 26);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(91, 20);
            this.comboBox1.TabIndex = 9;
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader3});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 49);
            this.listView1.Name = "listView1";
            this.listView1.SelectColumn = 0;
            this.listView1.Size = new System.Drawing.Size(410, 412);
            this.listView1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.listView1.SortToClick = true;
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "이름";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "PID";
            this.columnHeader2.Width = 50;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "창 핸들";
            this.columnHeader4.Width = 95;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "창 타이틀";
            this.columnHeader3.Width = 500;
            // 
            // frmProcessList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 461);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.chkProcessWindow);
            this.Controls.Add(this.menuStrip1);
            this.Name = "frmProcessList";
            this.Text = "프로세스 목록";
            this.Load += new System.EventHandler(this.frmProcessList_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새로고침RToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 맨위로고정ToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkProcessWindow;
        private Utils.HSListViewEx listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 스레드목록ToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 종료ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 종료강제로ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일시중지ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 프로세스재개ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}