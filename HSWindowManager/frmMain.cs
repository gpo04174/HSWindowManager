﻿using HSWindowManager.FindWindow;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HSWindowManager
{
    public partial class frmMain : Form
    {
        public const string Title = "HS™ 창 관리자";
        public static frmMain Instance;

        WindowItem window;
        public frmMain()
        {
            InitializeComponent();
            Instance = this;
            ts = new ThreadStart(AutoUpdate_Loop);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = Title;
            spyControl1.Grabbed += (object s, IntPtr Handle) => { try { if (checkBox2.Checked) getWindow(new WindowItem(Handle)); } catch { } };
            spyControl1.GrabFinished += (object s, IntPtr Handle) => { try { getWindow(new WindowItem(Handle)); } catch { } };
#if DEBUG
            btnWindowList.Enabled = true;
#endif
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            numWindowHandle.Hexadecimal = checkBox1.Checked;
        }

        private void btnWindowsRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                IntPtr Handle = new IntPtr((int)numWindowHandle.Value);
                try { Handle = (IntPtr)sender; } catch { }
                WindowItem window = new WindowItem(Handle);

                getWindow(window);
            }
            catch { MessageBox.Show("잘못된 창 핸들 입니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            getWindow(window);
        }


        private void btnFindWindow_Click(object sender, EventArgs e)
        {
            FindWindowForm fw = new FindWindowForm();
            fw.Grabbed += (object s, IntPtr h) => { try { if (checkBox2.Checked) getWindow(new WindowItem(fw.SelectedHandle)); } catch { } };
            if (fw.ShowDialog() == DialogResult.OK) try { getWindow(new WindowItem(fw.SelectedHandle)); } catch { }
        }


        #region 이벤트 및 창 제어부
        private void txtWindowTitle_KeyDown(object sender, KeyEventArgs e) { if (e.KeyCode == Keys.Enter) btnWindowTitle_Click(sender, null); }

        private void txtWindowTitle_TextChanged(object sender, EventArgs e) { if (!ControlLock && checkBox3.Checked) btnWindowTitle_Click(sender, e); }

        private void chkAOT_CheckedChanged(object sender, EventArgs e){ if (!ControlLock && window.IsPauseThread != BOOL.True) window.AlwaysOnTop = chkAOT.Checked; }
        private void chkWindowEnable_CheckedChanged(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.Enable = chkWindowEnable.Checked; }
        private void chkVisible_CheckedChanged(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.Visible = chkVisible.Checked; }
        private void btnWindowTitle_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) { window.Title = txtWindowTitle.Text; getWindow(window); } }
        private void tbWindowTransparent_ValueChanged(object sender, EventArgs e) { if(!ControlLock && window.IsPauseThread != BOOL.True) window.Transparent = (byte)tbWindowTransparent.Value; lblWindowTransparent.Text = tbWindowTransparent.Value.ToString(); }
        //private void chkWindowMouse_CheckedChanged(object sender, EventArgs e) { if (!ControlLock) window.MouseEnable = chkWindowMouse.Checked; }
        private void btnMinimize_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.Minimize(); }
        private void btnMaximize_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.Maximize(); }
        private void btnRestore_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.Restore(); }
        private void btnClose_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) { window.Close(); System.Threading.Thread.Sleep(500); getWindow(window); } }
        private void btnBlink_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) { spyControl1.HighlightWindow(window.WindowHandle); window.Blink(); } }
        private void btnBTF_Click(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.BringToFront(); }
        private void chkWindowClose_CheckedChanged(object sender, EventArgs e) { if (!ControlLock && window.IsPauseThread != BOOL.True) window.CloseButton = chkWindowClose.Checked ? BOOL.True : BOOL.False; }

        private void Area_ValueChanged(object sender, EventArgs e)
        {
            if(!ControlLock && window.IsPauseThread != BOOL.True)
            {
                int val = (int)((NumericUpDown)sender).Value;
                switch (((NumericUpDown)sender).Name)
                {
                    case "numAbsX":
                        window.WindowItemEx.DesktopLocation = new Point(val, window.WindowItemEx.DesktopLocation.Y);
                        //if(!chkWindowRefresh.Checked) getWindow(window, "numAbsX");
                        break;
                    case "numAbsY":
                        window.WindowItemEx.DesktopLocation = new Point(window.WindowItemEx.DesktopLocation.X, val);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numAbsY");
                        break;
                    case "numRelX":
                        window.WindowItemEx.Location = new Point(val, window.WindowItemEx.Location.Y);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numRelX");
                        break;
                    case "numRelY":
                        window.WindowItemEx.Location = new Point(window.WindowItemEx.Location.X, val);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numRelY");
                        break;
                    case "numDesktopWidth":
                        window.WindowItemEx.Size = new Size(val, window.WindowItemEx.Size.Height);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numDesktopWidth");
                        break;
                    case "numDesktopHeight":
                        window.WindowItemEx.Size = new Size(window.WindowItemEx.Size.Width, val);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numDesktopHeight");
                        break;
                    case "numClientWidth":
                        window.WindowItemEx.ClientSize = new Size(val, window.WindowItemEx.ClientSize.Height);
                        if (!chkWindowRefresh.Checked) getWindow(window, "numClientWidth");
                        break;
                    case "numClientHeight":
                        window.WindowItemEx.ClientSize = new Size(window.WindowItemEx.ClientSize.Width, val);
                        //if (!chkWindowRefresh.Checked) getWindow(window, "numClientHeight");
                        break;
                }
            }
        }
        #endregion

        Icon windowIcon;
        bool ControlLock = false;

        string Title_Before = null;
        public void getWindow(WindowItem Item, string ControlName = null)
        {
            ControlLock = true;

            try
            {
                Process pr = Item == null ? null : Item.Process;
                this.Text = pr == null ? Title : Title + string.Format(" - PID: {0} ({1})", pr.Id, pr.ProcessName);

                panel1.Enabled = Item.WindowItemEx.Exists;
                if (th == null &&  chkWindowRefresh.Checked &&  panel1.Enabled) AutoUpdateStart(); 


                lblWindowHandle.Text = string.Format("0x{0} ({1})", Item.WindowHandle.ToString("X8"), Item.WindowHandle.ToInt32().ToString());
                txtClass.Text = Item.Class;
                chkAOT.Checked = Item.AlwaysOnTop;
                chkVisible.Checked = Item.Visible;
                chkWindowEnable.Checked = Item.Enable;
                chkWindowClose.Checked = Item.CloseButton == BOOL.True;
                chkWindowClose.Enabled = Item.CloseButton != BOOL.Unknown;
                if(Title_Before != Item.Title) txtWindowTitle.Text = Title_Before = Item.Title;
                tbWindowTransparent.Value = Item.Transparent;

                if (ControlName != numAbsX.Name) numAbsX.Value = window.WindowItemEx.DesktopLocation.X;
                if (ControlName != numAbsY.Name) numAbsY.Value = window.WindowItemEx.DesktopLocation.Y;
                if (ControlName != numRelX.Name) numRelX.Value = window.WindowItemEx.Location.X;
                if (ControlName != numRelY.Name) numRelY.Value = window.WindowItemEx.Location.Y;
                if (ControlName != numDesktopWidth.Name) numDesktopWidth.Value = window.WindowItemEx.Size.Width;
                if (ControlName != numDesktopHeight.Name) numDesktopHeight.Value = window.WindowItemEx.Size.Height;
                if (ControlName != numClientWidth.Name) numClientWidth.Value = window.WindowItemEx.ClientSize.Width;
                if (ControlName != numClientHeight.Name) numClientHeight.Value = window.WindowItemEx.ClientSize.Height;

                //ProcessThread window.WindowItemEx.ThreadId)
                if (Item.IsPauseThread != BOOL.True)
                {
                    Icon icon = Item.WindowItemEx.IsHung ? Item.WindowItemEx.ClassIcon : Item.WindowItemEx.Icon;
                    if (icon == null) icon = Item.WindowItemEx.ClassIcon;
                    if (icon == null) icon = Item.WindowItemEx.IsHung ? Item.WindowItemEx.SmallClassIcon : Item.WindowItemEx.SmallIcon;
                    if (icon == null) icon = Item.WindowItemEx.SmallClassIcon;

                    if ((windowIcon == null || icon == null) || windowIcon.Handle != icon.Handle)
                    {
                        if (windowIcon != null) { windowIcon.Dispose(); windowIcon = null; }
                        if (this.pictureBox1.Image != null) { this.pictureBox1.Image.Dispose(); this.pictureBox1.Image = null; }
                        this.pictureBox1.Image = null;

                        windowIcon = icon;
                        this.pictureBox1.Image = icon != null ? icon.ToBitmap() : null;
                        if (this.pictureBox1.Image != null) label10.Text = string.Format("아이콘:\n({0} x {1})", this.pictureBox1.Image.Width, this.pictureBox1.Image.Height);
                        else label10.Text = "아이콘:";
                    }
                    else if (windowIcon.Handle == icon.Handle) icon.Dispose();
                }
                else
                {
                    if (window.Process.Id != Item.Process.Id)
                    {
                        if (windowIcon != null) { windowIcon.Dispose(); windowIcon = null; }
                        if (this.pictureBox1.Image != null) { this.pictureBox1.Image.Dispose(); this.pictureBox1.Image = null; }
                    }
                }
            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
            finally { ControlLock = false; this.window = Item; /*try { Application.DoEvents(); } catch { }*/ }
        }

        delegate void AutoUpdateInvoke();
        Thread th;
        ThreadStart ts;
        void AutoUpdateStart()
        {
            AutoUpdateStop();
            th = new Thread(ts);
            th.Start();
        }
        void AutoUpdateStop()
        {
            try { if (th != null) th.Abort(); } catch { }
            th = null;
        }
        //bool AutoUpdatePauseFlag;
        void AutoUpdate_Loop()
        {
            try
            {
                while (true)
                {
                    try
                    {
                        if (window.WindowItemEx.Exists)
                        {
                            if (this.InvokeRequired)
                                this.Invoke(new AutoUpdateInvoke(() => { getWindow(window); }));
                            else getWindow(window);
                        }
                    }
                    catch { }
                    Thread.Sleep(50);
                }
            }
            //ThreadAbortException
            catch { }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void 맨위로고정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위로고정ToolStripMenuItem.Checked;
        }
        
        private void chkWindowRefresh_CheckedChanged(object sender, EventArgs e)
        {
            //backgroundWorker1.CancelAsync();
            if (chkWindowRefresh.Checked) AutoUpdateStart();
            else AutoUpdateStop();
        }

        frmProcessList list_process;
        private void btnProcessList_Click(object sender, EventArgs e)
        {
            if(list_process == null || list_process.IsDisposed)list_process = new frmProcessList();
            list_process.Show();
            list_process.BringToFront();
        }

        private void btnWindowList_Click(object sender, EventArgs e)
        {

        }

        private void 아이콘저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = Process.GetProcessById((int)window.WindowItemEx.ProcessId).ProcessName;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileStream fs= null;
                try
                {
                    switch (saveFileDialog1.FilterIndex)
                    {
                        case 1: windowIcon.ToBitmap().Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Png); break;
                        default:
                            fs = new System.IO.FileStream(saveFileDialog1.FileName, System.IO.FileMode.Create);
                            windowIcon.Save(fs);
                            fs.Close();
                            break;
                    }
                }
                catch(Exception ex) { MessageBox.Show("저장에 실패하였습니다.\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); if (fs != null) fs.Close(); }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            아이콘저장ToolStripMenuItem.Enabled = windowIcon != null;

        }

        private void frmMain_SizeChanged(object sender, EventArgs e)
        {
            btnProcessList.Size = new Size((this.Width / 2) - 4, btnProcessList.Height);
            btnWindowList.Location = new Point((this.Width / 2) - 4, btnWindowList.Location.Y);
            btnWindowList.Size = new Size((this.Width / 2) - 13, btnWindowList.Height);
        }

        private void btnProcessGoTo_Click(object sender, EventArgs e)
        {
            if (list_process == null || list_process.IsDisposed) list_process = new frmProcessList(window.Process.Id);
            list_process.Show();
            list_process.BringToFront();
            list_process.SelectItem(window.Process.Id);
        }
    }
}
