﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;

namespace HSWindowManager.FindWindow
{
    public class EnumWindows
    {
        public List<Rectangle> reclist = new List<Rectangle>();

        private delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        private static class UnManagedMethods
        {
            [DllImport("user32.dll")]
            public static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);
            
            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, IntPtr lParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OnlyVisible">if true, find only window is visible</param>
        /// <returns></returns>
        public static void FindWindowRects(List<Rectangle> result, IntPtr thisWindow)
        {
            Win32.WindowInfo wInfo = new Win32.WindowInfo();
            Win32.Rect rect = new Win32.Rect();
            
            UnManagedMethods.EnumWindows(delegate(IntPtr wnd, IntPtr param)
            {
                if (wnd == thisWindow)  //except
                {
                    return true;
                }

                if (Win32.IsWindowVisible(wnd))
                {
                    //WindowInfo() 에서 WindowInfo를 받아올때 wInfo.client의 width,height가 rect의 오른쪽 아래 좌표임
                    Win32.GetWindowInfo(wnd, ref wInfo);
                    Rectangle rect1 = new Rectangle(wInfo.client.Left, wInfo.client.Top, Math.Abs(wInfo.client.Width - wInfo.client.Left), Math.Abs(wInfo.client.Height - wInfo.client.Top));

                    int insertIndex = result.Count;
                    UnManagedMethods.EnumChildWindows(wnd, delegate(IntPtr childHandle, IntPtr lparam)
                    {
                        //filtering a POPUP
                        Win32.WindowStyles style = Win32.GetWindowLong(childHandle, Win32.GWL_STYLE);
                        if (((UInt32)style & (UInt32)Win32.WindowStyles.WS_POPUP) != 0)
                            return true;
                        else if (!Win32.IsWindowVisible(childHandle))
                            return true;
                        else
                        {
                            //Win32.GetWindowRect(childHandle, ref rect);
                            Win32.GetWindowRect(childHandle, ref rect);
                            Rectangle rect2 = new Rectangle(rect.left, rect.top, rect.Width, rect.Height);

                            if (Rectangle.Equals(rect1, rect2) == false)
                            {
                                result.Insert(insertIndex, rect2);
                            }
                        }
                        return true;
                    }, IntPtr.Zero);

                    //popup이면 안 넣음
                    Win32.WindowStyles wnd_style = Win32.GetWindowLong(wnd, Win32.GWL_STYLE);
                    if (((UInt32)wnd_style & (UInt32)Win32.WindowStyles.WS_POPUP) != 0)
                    {
                        return true;
                    }
     
                    result.Add(rect1);
                }
                return true;
            }, IntPtr.Zero);

            return;
        }

        //public static void FindWindowRects2(Dictionary<IntPtr, Rectangle> result, IntPtr thisWindow)
        //{
        //    if (result == null)
        //    {
        //        throw new Exception("dictionary is null");
        //    }
        //    result.Clear();
        //    Win32.WindowInfo wInfo = new Win32.WindowInfo();
        //    Win32.Rect rect = new Win32.Rect();
        //    Win32.Rect clientRect = new Win32.Rect();

        //    UnManagedMethods.EnumWindows(delegate(IntPtr wnd, IntPtr param)
        //    {
        //        if (wnd == thisWindow)  //except
        //        {
        //            return true;
        //        }
                
        //        if(Win32.IsWindowVisible(wnd))
        //        {
        //            Win32.GetWindowInfo(wnd, ref wInfo);
        //            Win32.GetWindowRect(wnd, ref rect);
        //            Win32.GetClientRect(wnd, ref clientRect);

        //            if (0x104b8 == wnd.ToInt32())
        //            {
        //                int check = 0;
        //            }

        //            //Rectangle rect1 = new Rectangle(rect.left, rect.top, rect.Width, rect.Height);
        //            Rectangle rect1 = new Rectangle(wInfo.client.Left, wInfo.client.Top, wInfo.client.Width, wInfo.client.Height);

        //            int insertIndex = result.Count;
        //            UnManagedMethods.EnumChildWindows(wnd, delegate(IntPtr childHandle, IntPtr lparam)
        //            {
        //                //filtering a POPUP
        //                Win32.WindowStyles style = Win32.GetWindowLong(childHandle, Win32.GWL_STYLE);
        //                if (((UInt32)style & (UInt32)Win32.WindowStyles.WS_POPUP) != 0)
        //                    return true;
        //                else if (!Win32.IsWindowVisible(childHandle))
        //                    return true;
        //                else
        //                {
        //                    //Win32.GetWindowRect(childHandle, ref rect);
        //                    Win32.GetWindowRect(childHandle, ref rect);
        //                    Rectangle rect2 = new Rectangle(rect.left, rect.top, rect.Width, rect.Height);
        //                    if (Rectangle.Equals(rect1, rect2) == false)
        //                    {
        //                        result.Add(childHandle, rect2);
        //                    }
        //                }
        //                return true;
        //            }, IntPtr.Zero);

        //            result.Add(wnd, rect1);
        //        }
        //        return true;
        //    }, IntPtr.Zero);
        //}

    }
}
