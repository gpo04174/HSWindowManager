﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HSWindowManager.FindWindow
{
    public partial class FindWindowForm : Form
    {
        public event GrabbedEventHandler Grabbed;
        public event GrabbedEventHandler GrabbedFinish;

        public IntPtr SelectedHandle { get; private set; }

        private List<Rectangle> WindowRects;
        //private Dictionary<IntPtr, Rectangle> WindowRects2;
        private Rectangle _previousRect;
        private Rectangle selectedRect = Rectangle.Empty;
        private Rectangle size_text_rect;
        public Rectangle SelectedRect
        {
            get { return selectedRect; }
        }

        private Pen pen;
        private Point first, end;
        private Font font;
        private Color TransParentColor;
        private Color _sizeRectColor;
        private int size_text_padding = 1;

        private Rectangle fullScrenn_bounds;

        public Color SizeRectColor
        {
            get { return _sizeRectColor; }
            set { _sizeRectColor = value; }
        }

        public FindWindowForm()
        {
            InitializeComponent();

            //this.WindowState = FormWindowState.Maximized;

            fullScrenn_bounds = Rectangle.Empty;

            foreach (var screen in Screen.AllScreens)
            {
                fullScrenn_bounds = Rectangle.Union(fullScrenn_bounds, screen.Bounds);
            }
            this.ClientSize = new Size(fullScrenn_bounds.Width, fullScrenn_bounds.Height);
            this.Location = new Point(fullScrenn_bounds.Left, fullScrenn_bounds.Top);

            first = new Point();
            end = new Point();

            pen = new Pen(new SolidBrush(Color.Orange), 2);
            font = new System.Drawing.Font("Verdana", 10);

            SizeRectColor = Color.Orange;

            TransParentColor = Color.Turquoise;
            this.TransparencyKey = TransParentColor;

            WindowRects = new List<Rectangle>();
            EnumWindows.FindWindowRects(WindowRects, this.Handle);
            
            //WindowRects2 = new Dictionary<IntPtr, Rectangle>();
            //EnumWindows.EnumWindows.FindWindowRects2(WindowRects2, this.Handle);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                this.Close();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (!selectedRect.Equals(Rectangle.Empty))  //선택 후
            {
                #region Draw Area
                e.Graphics.FillRectangle(new SolidBrush(TransParentColor), selectedRect);
                //e.Graphics.DrawRectangle(orangePen, selectArea);
                e.Graphics.DrawRectangle(pen, selectedRect.X - pen.Width, selectedRect.Y - pen.Width, selectedRect.Width + pen.Width * 2, selectedRect.Height + pen.Width * 2);
                #endregion

                #region Draw sizeString

                //String areaInfo = string.Format("{0} x {1}", rightbottom.X - leftup.X, rightbottom.Y - leftup.Y);
                String areaInfo = string.Format("{0} x {1}", selectedRect.Width, selectedRect.Height);
                //String areaInfo = string.Format("{0}", selectArea);
                SizeF areaInfoSize = e.Graphics.MeasureString(areaInfo, font);
                

                if (selectedRect.Right + areaInfoSize.Width + size_text_padding * 2 > fullScrenn_bounds.Width)//안쪽에 그릴때
                {
                    size_text_rect = new Rectangle(
                            (int)(selectedRect.Right - areaInfoSize.Width - size_text_padding),
                            selectedRect.Bottom - (int)(areaInfoSize.Height + pen.Width + size_text_padding),
                            (int)areaInfoSize.Width,
                            (int)areaInfoSize.Height);

                    e.Graphics.FillRectangle(new SolidBrush(SizeRectColor), size_text_rect);
                    e.Graphics.DrawString(areaInfo, font, new SolidBrush(Color.White), size_text_rect);
                }
                else//바깥쪽에 그릴 때
                {
                    size_text_rect = new Rectangle(selectedRect.Right + 5, selectedRect.Bottom - (int)(areaInfoSize.Height + pen.Width + size_text_padding), (int)areaInfoSize.Width, (int)areaInfoSize.Height);
                    e.Graphics.FillRectangle(new SolidBrush(SizeRectColor), size_text_rect);
                    e.Graphics.DrawString(areaInfo, font, new SolidBrush(Color.White), size_text_rect);
                }
                #endregion
            }

            if (first.X != end.X && first.Y != end.Y) //선택하기 전
            {
                //Set Area
                Point leftup = new Point(Math.Min(first.X, end.X), Math.Min(first.Y, end.Y));
                Point rightbottom = new Point(Math.Max(first.X, end.X), Math.Max(first.Y, end.Y));
                Rectangle selectArea = new Rectangle(leftup.X, leftup.Y, rightbottom.X - leftup.X, rightbottom.Y - leftup.Y);

                #region Draw Area
                e.Graphics.FillRectangle(new SolidBrush(TransParentColor), selectArea);
                //e.Graphics.DrawRectangle(orangePen, selectArea);
                e.Graphics.DrawRectangle(pen, selectArea.X - pen.Width, selectArea.Y - pen.Width, selectArea.Width + pen.Width * 2, selectArea.Height + pen.Width * 2);
                #endregion

                #region Draw sizeString
                
                String areaInfo = string.Format("{0} x {1}", rightbottom.X - leftup.X, rightbottom.Y - leftup.Y);
                //String areaInfo = string.Format("{0}", selectArea);
                SizeF areaInfoSize = e.Graphics.MeasureString(areaInfo, font);
                int padding = 1;

                //System.Diagnostics.Debug.WriteLine("rightbottom.X : {0} stringSize.Width :{1} fullScreen_Right : {2}", rightbottom.X, areaInfoSize.Width, fullScrenn_bounds.Right);
                if (selectArea.Right + areaInfoSize.Width + padding * 2 > fullScrenn_bounds.Width)//안쪽에 그릴때
                {
                    e.Graphics.FillRectangle(
                        new SolidBrush(SizeRectColor),
                        new Rectangle(
                            (int)(rightbottom.X - areaInfoSize.Width - padding),
                            rightbottom.Y - (int)(areaInfoSize.Height + pen.Width + padding),
                            (int)areaInfoSize.Width,
                            (int)areaInfoSize.Height));
                    e.Graphics.DrawString(
                        areaInfo,
                        font,
                        new SolidBrush(Color.White),
                        new Point(
                            (int)(rightbottom.X - areaInfoSize.Width - padding),
                            rightbottom.Y - (int)(areaInfoSize.Height + pen.Width + padding)));
                }
                else//바깥쪽에 그릴 때
                {
                    e.Graphics.FillRectangle(new SolidBrush(SizeRectColor), new Rectangle(rightbottom.X + 5, rightbottom.Y - (int)(areaInfoSize.Height + pen.Width + padding), (int)areaInfoSize.Width, (int)areaInfoSize.Height));
                    e.Graphics.DrawString(areaInfo, font, new SolidBrush(Color.White), new Point(rightbottom.X + 5, rightbottom.Y - (int)(areaInfoSize.Height + pen.Width + padding)));
                } 
                #endregion
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (first.X == end.X && first.Y == end.Y) selectedRect = curRect;
            else selectedRect = new Rectangle(Math.Min(first.X, end.X), Math.Min(first.Y, end.Y), Math.Abs(first.X - end.X), Math.Abs(first.Y - end.Y));

            //end.X = first.X = e.X;
            //end.Y = first.Y = e.Y;

            SelectedHandle = LastPtr;
            try { GrabbedFinish?.Invoke(this, LastPtr); } catch { }

            DialogResult = DialogResult.OK;
            this.Close();
        }

        IntPtr curWin, LastPtr;
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                end.X = e.X;
                end.Y = e.Y;

                //DialogResult = DialogResult.OK;
                //this.Close();
            }
            else if (Rectangle.Equals(selectedRect, Rectangle.Empty))
            {
                foreach (var window in WindowRects)
                {
                    if (window.Contains(Cursor.Position))
                    {
                        if (_previousRect != null)
                        {
                            if (Rectangle.Equals(_previousRect, window))//skip
                            {
                                return;
                            }
                        }

                        _previousRect = window;

                        //form안의 좌표 <-> screen 좌표
                        first.X = window.X;
                        first.Y = window.Y;

                        int x = ClientRectangle.Left - fullScrenn_bounds.Left;
                        int y = ClientRectangle.Top - fullScrenn_bounds.Top;

                        first.X += x;
                        first.Y += y;

                        end.X = first.X + window.Width;
                        end.Y = first.Y + window.Height;

                        curRect = new Rectangle(Math.Min(first.X, end.X), Math.Min(first.Y, end.Y), Math.Abs(first.X - end.X), Math.Abs(first.Y - end.Y));

                        System.Diagnostics.Debug.WriteLine(string.Format(" window Rect : ({0},{1}) [{2} X {3}]", window.X, window.Y, window.Width, window.Height));

                        Win32.POINT pt = e.Location;
                        Win32.ClientToScreen(this.Handle, ref pt);
                        curWin = Win32.WindowFromPoint(pt);
                        if (LastPtr != curWin)
                        {
                            LastPtr = curWin;
                            try { Grabbed?.Invoke(this, LastPtr); } catch { }
                        }

                        this.Refresh();
                        break;
                    }

                }

            }
        }

        Rectangle curRect;
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (first.X == end.X && first.Y == end.Y)
            {
                selectedRect = curRect;
            }
            else
            {
                selectedRect = new Rectangle(Math.Min(first.X, end.X), Math.Min(first.Y, end.Y), Math.Abs(first.X - end.X), Math.Abs(first.Y - end.Y));
            }

            this.MouseDown -= Form1_MouseDown;
            this.MouseMove -= Form1_MouseMove;

            this.Refresh();
        }
    }


}
