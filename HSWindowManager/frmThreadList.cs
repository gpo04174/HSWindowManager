﻿using HSWindowManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HSWindowManager
{
    public partial class frmThreadList : Form
    {
        internal frmThreadList()
        {
            InitializeComponent();
        }
        Process process;
        string processName;
        public frmThreadList(Process process)
        {
            this.process = process;
            this.processName = process.ProcessName;
            InitializeComponent();
        }

        string msg_ex;
        List<ThreadPack> thread = new List<ThreadPack>();
        void RefreshThread()
        {
            process.Refresh();
            thread.Clear();

            try
            {
                if (!process.HasExited)
                {
                    ProcessThreadCollection col = process.Threads;
                    foreach (ProcessThread th in col)
                    {
                        List<WindowItemEx> list = WindowItemEx.GetThreadWindows((uint)th.Id);
                        thread.Add(new ThreadPack(th, list));
                    }
                }
                else
                {
                    timer1.Stop();
                    새로고침RToolStripMenuItem.Enabled = false;
                    label1.Text = "프로세스" + " (PID: " + process.Id + ")[" + processName + "]" + " 가 종료되었습니다.\n\n(클릭하면 창이 닫힙니다.)";
                    listView1.Visible = false;
                    label1.Visible = true;
                }
            }
            catch(Exception ex) { msg_ex = ex.Message; }
        }

        void RefreshList(bool Focus = false)
        {
            ThreadPack sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0].Tag as ThreadPack : null;
            RefreshList(process.Id);

            if(thread.Count != 0)
            {
                listView1.Sort();
                if (Focus)
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        ThreadPack sel1 = listView1.Items[i].Tag as ThreadPack;
                        if (sel1.thread.Id == sel.thread.Id)
                        {
                            listView1.EnsureVisible(i);
                            break;
                        }
                    }
                listView1.Select();
            }
        }
        int RefreshList(int PID)
        {
            int select = -1;

            listView1.BeginUpdate();
            listView1.Items.Clear();
            RefreshThread();

            if (thread.Count > 0)
            {
                List<ListViewItem> li = new List<ListViewItem>();
                for (int i = 0; i < thread.Count; i++)
                {
                    if (!checkBox1.Checked || thread[i].window.Count > 0)
                    {
                        try
                        {
                            ListViewItem item = new ListViewItem(thread[i].thread.Id.ToString());//("0x" + th.Id.ToString("X8"));
                            item.Tag = thread[i];
                            item.SubItems.Add(thread[i].window.Count > 0 ? thread[i].window.Count.ToString() + " 개" : "");
                            item.SubItems.Add(thread[i].thread.ThreadState.ToString() + (thread[i].thread.ThreadState == ThreadState.Wait ? " (" + thread[i].thread.WaitReason + ")" : "" ));
                            item.SubItems.Add(thread[i].thread.PriorityLevel.ToString());
                            item.SubItems.Add(thread[i].thread.StartTime.ToString());
                            if(thread[i].thread.ThreadState == ThreadState.Wait && thread[i].thread.WaitReason == ThreadWaitReason.Suspended)
                            {
                                item.BackColor = Program.SuspendListViewBackColor;
                                item.ForeColor = Program.SuspendListViewItemColor;
                            }
                            else
                            {
                                item.BackColor = Program.NormalListViewBackColor;
                                item.ForeColor = Program.NormalListViewItemColor;
                            }
                            li.Add(item);
                            if (thread[i].thread.Id == PID)
                            {
                                select = i;
                                item.Focused = true;
                                item.Focused = true;
                            }
                        }
                        catch { }
                    }
                }
                listView1.Items.AddRange(li.ToArray());
                if (select > -1 && select < listView1.Items.Count)
                {
                    //listView1.EnsureVisible(select);
                }
                listView1.EndUpdate();

                if (li.Count == 0) checkBox1.Checked = false;
                checkBox1.Enabled = li.Count > 0;
                li.Clear();
            }
            else
            {
                timer1.Stop();
                새로고침RToolStripMenuItem.Enabled = false;
                label1.Text = "프로세스 [" + processName + "]" + "(PID: " + process.Id + ")" + 
                    (msg_ex != null ? " 의\n스레드를 가져오던 중 오류가 발생하였습니다.\n\n[이유: " + msg_ex +"]" : " 가 종료되었습니다." ) + 
                    "\n\n\n(클릭하면 창이 닫힙니다.)";
                listView1.Visible = false;
                label1.Visible = true;
            }

            return select;
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                ThreadPack pack = listView1.SelectedItems[0].Tag as ThreadPack;
                if (pack.window != null && pack.window.Count > 0) new frmWindowList(pack.thread, process, pack.window).Show();
                else MessageBox.Show("창이 존재하지 않는 스레드 입니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e) {if (e.KeyCode == Keys.Enter) listView1_MouseDoubleClick(this, null); }

        private void ThreadWindow_Load(object sender, EventArgs e)
        {
            this.Text = "스레드 목록 (PID: " + process.Id + ") [" + process.ProcessName + "]";
            새로고침RToolStripMenuItem_Click(this, e);
            toolStripComboBox1.SelectedIndex = 0;
        }

        private void 새로고침RToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ThreadPack sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0].Tag as ThreadPack : null;

            int scrollSpot = listView1.TopItem == null ? 0 : listView1.TopItem.Index;
            RefreshList(sel == null ? -1 : sel.thread.Id);
            if(listView1.Items.Count > 0)listView1.TopItem = listView1.Items[scrollSpot];
        }

        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThreadList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) listView1_MouseDoubleClick(this, null);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ThreadPack sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0].Tag as ThreadPack : null;

            listView1.BeginUpdate();

            listView1.Items.Clear();

            List<ListViewItem> li = new List<ListViewItem>();
            int select = -1;
            for (int i = 0; i < thread.Count; i++)
            {
                if (!checkBox1.Checked || thread[i].window.Count > 0)
                {
                    try
                    {
                        ListViewItem item = new ListViewItem(thread[i].thread.Id.ToString());//("0x" + th.Id.ToString("X8"));
                        item.Tag = thread[i];
                        item.SubItems.Add(thread[i].window.Count > 0 ? thread[i].window.Count.ToString() + " 개" : "");
                        item.SubItems.Add(thread[i].thread.ThreadState.ToString() + (thread[i].thread.ThreadState == ThreadState.Wait ? " (" + thread[i].thread.WaitReason + ")" : "" ));
                        item.SubItems.Add(thread[i].thread.PriorityLevel.ToString());
                        item.SubItems.Add(thread[i].thread.StartTime.ToString());
                        if (thread[i].thread.ThreadState == ThreadState.Wait && thread[i].thread.WaitReason == ThreadWaitReason.Suspended)
                        {
                            item.BackColor = Program.SuspendListViewBackColor;
                            item.ForeColor = Program.SuspendListViewItemColor;
                        }
                        else
                        {
                            item.BackColor = Program.NormalListViewBackColor;
                            item.ForeColor = Program.NormalListViewItemColor;
                        }
                        li.Add(item);
                        if (sel != null && thread[i].thread.Id == sel.thread.Id) select = i;
                    }
                    catch{  }
                }
            }
            listView1.Items.AddRange(li.ToArray());
            if (select > -1 && select < listView1.Items.Count)
            {
                listView1.EnsureVisible(select);
                listView1.Items[select].Focused = true;
                listView1.Items[select].Selected = true;
            }
            li.Clear();

            listView1.EndUpdate();
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 1: timer1.Interval = 2000; timer1.Start(); break;
                case 2: timer1.Interval = 1000; timer1.Start(); break;
                case 3: timer1.Interval = 500; timer1.Start(); break;
                case 4: timer1.Interval = 250; timer1.Start(); break;
                default: timer1.Stop(); break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e) { 새로고침RToolStripMenuItem_Click(sender, e); }

        private void 맨위로고정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){this.TopMost = 맨위로고정ToolStripMenuItem.Checked; }

        private class ThreadPack
        {
            public ThreadPack(ProcessThread thread, List<WindowItemEx> window) { this.thread = thread; this.window = window; }
            public ProcessThread thread;
            public List<WindowItemEx> window = null;
        }

        #region 스레드 관리
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                ThreadPack tp = listView1.SelectedItems[0].Tag as ThreadPack;
                창목록ToolStripMenuItem.Enabled = tp.window.Count > 0;
            }
            else 창목록ToolStripMenuItem.Enabled = false;
            스레드일시정지ToolStripMenuItem.Enabled = 스레드재개ToolStripMenuItem.Enabled = 스레드종료ToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
        }

        private void 창목록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1_MouseDoubleClick(this, null);
        }
        private void 스레드일시정지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int err = 0;
            BOOL result = BOOL.False;

            bool IsWow64 = KernelAPI.ProcessIsWow64(process);
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                ThreadPack tp = listView1.SelectedItems[i].Tag as ThreadPack;
                if (tp != null)
                {
                    result = KernelAPI.ThreadSuspend(tp.thread.Id, IsWow64);
                    if (result != BOOL.True) err++;
                    //if (count > -1) listView1.SelectedItems[i].BackColor = Program.GrayListViewItemColor;
                }
            }

            if (err > 0)
                if (listView1.SelectedItems.Count > 1) MessageBox.Show("스레드 일시정지의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.False) MessageBox.Show("스레드를 일시정지하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Unknown) MessageBox.Show("알 수 없는 오류로 스레드를 일시정지하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Denied) MessageBox.Show("액세스가 거부되었습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else MessageBox.Show("특정한 오류로 스레드를 일시정지하지 못했습니다. (코드: " + (int)result + ")", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            RefreshList(true);
        }
        private void 스레드재개ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int err = 0;
            BOOL result = BOOL.False;

            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                ThreadPack tp = listView1.SelectedItems[i].Tag as ThreadPack;
                if (tp != null)
                {
                    result = KernelAPI.ThreadResume(tp.thread.Id);
                    if (result != BOOL.True) err++;
                    //if (count > -1) listView1.SelectedItems[i].BackColor = Program.NormalListViewItemColor;
                }
            }

            if (err > 0)
                if (listView1.SelectedItems.Count > 1) MessageBox.Show("스레드 재개의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if(result == BOOL.False)MessageBox.Show("스레드를 재개하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Unknown) MessageBox.Show("알 수 없는 오류로 스레드를 재개하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Denied) MessageBox.Show("액세스가 거부되었습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else MessageBox.Show("특정한 오류로 스레드를 재개하지 못했습니다. (코드: " + (int)result + ")", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            RefreshList(true);
        }
        private void 스레드종료ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("스레드를 종료하는것은 프로그램에 악 영향이 있을 수 있습니다. \n\n정말로 스레드를 종료하시겠습니까?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                int err = 0;
                BOOL result = BOOL.False;

                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    ThreadPack sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[i].Tag as ThreadPack : null;
                    if(sel != null) result = KernelAPI.ThreadTerminate(sel.thread.Id);
                    if (result != BOOL.True) err++;
                }

                if (err > 0)
                    if (listView1.SelectedItems.Count > 1) MessageBox.Show("스레드 종료의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (result == BOOL.False) MessageBox.Show("스레드를 종료하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (result == BOOL.Unknown) MessageBox.Show("알 수 없는 오류로 스레드를 종료하지 못했습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (result == BOOL.Denied) MessageBox.Show("액세스가 거부되었습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else MessageBox.Show("특정한 오류로 스레드를 종료하지 못했습니다. (코드: " + (int)result + ")", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

                System.Threading.Thread.Sleep(200);
                RefreshThread();
                RefreshList();
            }
        }
        #endregion
    }
}
