﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace HSWindowManager
{
    static class Program
    {
        public static Color NormalListViewBackColor { get { return SystemColors.Window; } }
        public static Color NormalListViewItemColor { get { return SystemColors.ControlText; } }
        public static Color SuspendListViewBackColor { get { return SystemColors.GrayText; } }
        public static Color SuspendListViewItemColor { get { return SystemColors.ControlLightLight; } }

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
