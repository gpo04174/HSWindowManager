﻿using HSWindowManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HSWindowManager
{
    public partial class frmProcessList : Form
    {
        public frmProcessList()
        {
            InitializeComponent();
        }
        public frmProcessList(int PID)
        {
            InitializeComponent();

            int select = RefreshList(true, PID);
            if (select > -1 && select < listView1.Items.Count)
                listView1.EnsureVisible(select);
            listView1.Sort(0, SortOrder.Ascending);
            listView1.Select();
        }

        #region 메서드
        Process[] pr = null;
        int RefreshList(bool New, int PID = -1)
        {
            if (New) pr = Process.GetProcesses();

            listView1.Items.Clear();

            List<ListViewItem> list = new List<ListViewItem>();
            int select = -1, j = 0;
            for (int i = 0; i < pr.Length; i++)
            {
                try
                {
                    if (!chkProcessWindow.Checked || (pr[i].MainWindowHandle != IntPtr.Zero))
                    {

                        // + (KernelAPI.ProcessIsWow64(pr[i]) ? "" : " *")
                        ListViewItem li = new ListViewItem(pr[i].ProcessName);
                        li.SubItems.Add(pr[i].Id.ToString());
                        li.SubItems.Add(pr[i].MainWindowHandle == IntPtr.Zero ? "" : string.Format("0x{0} ({1})", pr[i].MainWindowHandle.ToString("X8"), pr[i].MainWindowHandle.ToString()));
                        li.SubItems.Add(pr[i].MainWindowTitle);
                        li.BackColor = IsProcessRunning(pr[i]) ? Program.NormalListViewBackColor : Program.SuspendListViewBackColor;
                        li.ForeColor = IsProcessRunning(pr[i]) ? Program.NormalListViewItemColor : Program.SuspendListViewItemColor;
                        li.Tag = pr[i];
                        if (pr[i].Id == PID)
                        {
                            select = j;
                            li.Focused = true;
                            li.Selected = true;
                        }
                        list.Add(li);
                        j++;
                    }
                }
                catch { }
            }

            /*
            int cnt = Math.Max(listView1.Items.Count, list.Count);
            //순차적 삽입으로 Flicker 방지
            for(int i = 0; i < cnt; i++)
            {
                //갯수가 아이템 목록보다 클때 (추가해야됨)
                if (list.Count > listView1.Items.Count)
                {
                    if (i < listView1.Items.Count) listView1.Items[i] = list[i];
                    else listView1.Items.Add(list[i]);
                }
                //갯수가 아이템 목록보다 작을때 (지워야됨)
                else
                {
                    if (i < list.Count) listView1.Items[i] = list[i];
                    else { listView1.Items.RemoveAt(listView1.Items.Count - 1);}
                }
            }
            */
            listView1.Items.AddRange(list.ToArray());
            list.Clear();
            return select;
        }

        public void SelectItem(int PID, bool Focusing = true)
        {
            int id = 0;
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                Process sel = listView1.Items[i].Tag as Process;
                if (sel.Id == PID)
                {
                    listView1.Items[i].Focused = true;
                    listView1.Items[i].Selected = true;
                    id = i;
                }
                else listView1.Items[i].Selected = false;
            }

            if (Focusing) listView1.EnsureVisible(id);
            listView1.Select();
        }


        void Refresh_(bool Focus = false)
        {
            Process sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0].Tag as Process : null;

            listView1.BeginUpdate();

            int scrollSpot = listView1.TopItem == null ? 0 : listView1.TopItem.Index;
            //WindowAPIEx.LockWindowUpdate(listView1.Handle);
            //int horzScroll = WindowAPIEx.GetScrollPos(listView1.Handle, WindowAPIEx.SBS_VERT);
            RefreshList(false);
            listView1.Sort();
            listView1.EndUpdate();

            SelectItem(sel == null ? -1 :sel.Id, Focus);

            //WindowAPIEx.SendMessage(listView1.Handle, WindowAPIEx.LVM_SCROLL, (IntPtr)scrollSpot, IntPtr.Zero);
            //WindowAPIEx.LockWindowUpdate(IntPtr.Zero);

            // restore index and top positions
            //if (listView1.Items.Count > scrollSpot) listView1.TopItem = listView1.Items[scrollSpot];
        }
        #endregion


        private void 새로고침RToolStripMenuItem_Click(object sender, EventArgs e) { Refresh_();}
        private void chkProcessWindow_CheckedChanged(object sender, EventArgs e) { Refresh_(true);}


        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (frmMain.Instance != null)
            {
                if (listView1.SelectedItems != null || listView1.SelectedItems.Count > 0)
                {
                    Process pr = listView1.SelectedItems[0].Tag as Process;
                    try
                    {
                        if (pr.MainWindowHandle != IntPtr.Zero) { frmMain.Instance.getWindow(pr.MainWindowHandle); frmMain.Instance.BringToFront(); }
                        else { MessageBox.Show("창 핸들이 없습니다."+(chkProcessWindow.Checked ? "\n\n새로고침을 해주시기 바랍니다." : ""), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information); }
                    }
                    catch { MessageBox.Show("프로세스가 종료되었습니다.\n\n새로고침을 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information); }
                }
            }
        }

        private void frmProcessList_Load(object sender, EventArgs e)
        {
            RefreshList(true);
            toolStripComboBox1.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
        }
        private void listView1_KeyDown(object sender, KeyEventArgs e) { if (e.KeyCode == Keys.Enter) listView1_MouseDoubleClick(this, null); }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 1: timer1.Interval = 2000; timer1.Start(); break;
                case 2: timer1.Interval = 1000; timer1.Start(); break;
                case 3: timer1.Interval = 500; timer1.Start(); break;
                case 4: timer1.Interval = 250; timer1.Start(); break;
                default: timer1.Stop(); break;
            }
        }

        private void 스레드목록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process pr = listView1.SelectedItems[0].Tag as Process;
            if (pr != null)
            {
                frmThreadList tw = new frmThreadList(pr);
                tw.Show();
                tw.BringToFront();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            스레드목록ToolStripMenuItem.Enabled = listView1.SelectedItems.Count == 1;
            종료강제로ToolStripMenuItem.Enabled = 프로세스재개ToolStripMenuItem.Enabled = 일시중지ToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
            if (listView1.SelectedItems.Count > 0)
            {
                try
                {
                    Process p = listView1.SelectedItems[0].Tag as Process;
                    if (!p.HasExited) 종료ToolStripMenuItem.Enabled = p.MainWindowHandle != IntPtr.Zero;
                    else MessageBox.Show("프로세스가 종료되었습니다.\n\n새로고침을 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch { }
                //catch(Exception ex){ MessageBox.Show("알 수 없는 오류가 발생였습니다.\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information); }
            }
            else 종료ToolStripMenuItem.Enabled = false;
        }

        private void 종료강제로ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("강제로 프로그램을 끝내게되면 저장되지 않은 작업은 잃습니다.\n\n정말로 종료하시겠습니까?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    Process p = listView1.SelectedItems[0].Tag as Process;
                    p.Kill();
                    System.Threading.Thread.Sleep(500);
                    RefreshList(true);
                }
                catch { }
            }
        }

        private void 종료ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process p = listView1.SelectedItems[0].Tag as Process;
            WindowItem window = p.MainWindowHandle;
            window.Close();
            System.Threading.Thread.Sleep(500);
            RefreshList(true);
        }

        private void 일시중지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int err = 0;
            BOOL result = BOOL.Unknown;
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                Process pr = listView1.SelectedItems[i].Tag as Process;
                result = ProcessSuspend(pr);
                if (result != BOOL.True) err++;
                //listView1.SelectedItems[i].BackColor = result == BOOL.True ? Program.GrayListViewItemColor : Program.NormalListViewItemColor;
            }

            if (err > 0)
                if (listView1.SelectedItems.Count > 1) MessageBox.Show("스레드 재개의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Unknown) MessageBox.Show("알 수 없는 오류로 스레드 재개의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Denied) MessageBox.Show("액세스가 거부되었습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            RefreshList(true);
        }

        private void 프로세스재개ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int err = 0;
            BOOL result = BOOL.Unknown;
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                Process pr = listView1.SelectedItems[i].Tag as Process;
                result = ProcesResume(pr);
                if (result != BOOL.True) err++;
                //listView1.SelectedItems[i].BackColor = result == BOOL.True ? Program.NormalListViewItemColor : Program.GrayListViewItemColor;
            }

            if (err > 0)
                if (listView1.SelectedItems.Count > 1) MessageBox.Show("스레드 재개의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if(result == BOOL.Unknown) MessageBox.Show("알 수 없는 오류로 스레드 재개의 일부가 실패하였습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (result == BOOL.Denied) MessageBox.Show("액세스가 거부되었습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            RefreshList(true);
        }

        #region
        public static bool IsProcessRunning(Process process)
        {
            bool suspend = true;
            ProcessThreadCollection col = process.Threads;
            for (int i = 0; i < col.Count; i++)
                if (col[i].ThreadState != ThreadState.Wait || col[i].WaitReason != ThreadWaitReason.Suspended) { suspend = false; break; }
            return !suspend;
        }

        public static BOOL ProcessSuspend(Process process)
        {
            BOOL result = BOOL.Unknown;
            bool IsWow64 = false;
            try { IsWow64 = KernelAPI.ProcessIsWow64(process); } catch { }
            ProcessThreadCollection col = process.Threads;
            for (int i = 0; i < col.Count; i++)
            {
                try
                {
                    result = KernelAPI.ThreadSuspend(col[i].Id, IsWow64);
                    //if (result != BOOL.True) return result;
                }
                catch { }
            }
            return result;
        }
        public static BOOL ProcesResume(Process process)
        {
            BOOL result = BOOL.Unknown;
            ProcessThreadCollection col = process.Threads;
            for (int i = 0; i < col.Count; i++)
            {
                try
                {
                    result = KernelAPI.ThreadResume(col[i].Id);
                    //if (result != BOOL.True) return result;
                }
                catch { }
            }
            return result;
        }
        #endregion

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            switch(comboBox1.SelectedIndex)
            {

            }
        }
    }
}
