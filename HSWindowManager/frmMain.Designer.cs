﻿namespace HSWindowManager
{
    partial class frmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            AutoUpdateStop();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.spyControl1 = new HSWindowManager.Spy.SpyControl();
            this.btnFindWindow = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.numWindowHandle = new System.Windows.Forms.NumericUpDown();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnProcessGoTo = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.아이콘저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label10 = new System.Windows.Forms.Label();
            this.chkWindowRefresh = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkWindowClose = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.chkAOT = new System.Windows.Forms.CheckBox();
            this.chkWindowEnable = new System.Windows.Forms.CheckBox();
            this.chkVisible = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnMaximize = new System.Windows.Forms.Button();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnBlink = new System.Windows.Forms.Button();
            this.btnBTF = new System.Windows.Forms.Button();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numAbsY = new System.Windows.Forms.NumericUpDown();
            this.numAbsX = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numRelY = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numRelX = new System.Windows.Forms.NumericUpDown();
            this.numDesktopHeight = new System.Windows.Forms.NumericUpDown();
            this.numDesktopWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numClientHeight = new System.Windows.Forms.NumericUpDown();
            this.numClientWidth = new System.Windows.Forms.NumericUpDown();
            this.lblWindowTransparent = new System.Windows.Forms.Label();
            this.btnWindowRefresh = new System.Windows.Forms.Button();
            this.lblWindowHandle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbWindowTransparent = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.btnWindowTitle = new System.Windows.Forms.Button();
            this.txtWindowTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnWindowsRefresh = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.끝내기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로고정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnProcessList = new System.Windows.Forms.Button();
            this.btnWindowList = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWindowHandle)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAbsY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAbsX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRelY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRelX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDesktopHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDesktopWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numClientHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numClientWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWindowTransparent)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.spyControl1);
            this.groupBox1.Controls.Add(this.btnFindWindow);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.numWindowHandle);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.btnWindowsRefresh);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(2, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(570, 281);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "창 옵션";
            // 
            // spyControl1
            // 
            this.spyControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spyControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("spyControl1.BackgroundImage")));
            this.spyControl1.Location = new System.Drawing.Point(445, 13);
            this.spyControl1.MaximumSize = new System.Drawing.Size(31, 28);
            this.spyControl1.MinimumSize = new System.Drawing.Size(31, 28);
            this.spyControl1.Name = "spyControl1";
            this.spyControl1.Size = new System.Drawing.Size(31, 28);
            this.spyControl1.TabIndex = 1;
            this.spyControl1.TopMost = true;
            // 
            // btnFindWindow
            // 
            this.btnFindWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindWindow.Location = new System.Drawing.Point(379, 16);
            this.btnFindWindow.Name = "btnFindWindow";
            this.btnFindWindow.Size = new System.Drawing.Size(60, 21);
            this.btnFindWindow.TabIndex = 20;
            this.btnFindWindow.Text = "창 크기";
            this.btnFindWindow.UseVisualStyleBackColor = true;
            this.btnFindWindow.Click += new System.EventHandler(this.btnFindWindow_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(489, 18);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(76, 16);
            this.checkBox2.TabIndex = 19;
            this.checkBox2.Text = "즉시 잡기";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // numWindowHandle
            // 
            this.numWindowHandle.Hexadecimal = true;
            this.numWindowHandle.Location = new System.Drawing.Point(35, 17);
            this.numWindowHandle.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numWindowHandle.Minimum = new decimal(new int[] {
            -2147483648,
            0,
            0,
            -2147483648});
            this.numWindowHandle.Name = "numWindowHandle";
            this.numWindowHandle.Size = new System.Drawing.Size(137, 21);
            this.numWindowHandle.TabIndex = 18;
            this.numWindowHandle.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(175, 20);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(46, 16);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "Hex";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnProcessGoTo);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.chkWindowRefresh);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.txtClass);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.lblWindowTransparent);
            this.panel1.Controls.Add(this.btnWindowRefresh);
            this.panel1.Controls.Add(this.lblWindowHandle);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbWindowTransparent);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnWindowTitle);
            this.panel1.Controls.Add(this.txtWindowTitle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(2, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 233);
            this.panel1.TabIndex = 15;
            // 
            // btnProcessGoTo
            // 
            this.btnProcessGoTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcessGoTo.Location = new System.Drawing.Point(305, 3);
            this.btnProcessGoTo.Name = "btnProcessGoTo";
            this.btnProcessGoTo.Size = new System.Drawing.Size(30, 24);
            this.btnProcessGoTo.TabIndex = 40;
            this.btnProcessGoTo.Text = "Pr";
            this.btnProcessGoTo.UseVisualStyleBackColor = true;
            this.btnProcessGoTo.Click += new System.EventHandler(this.btnProcessGoTo_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(545, 66);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 39;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Location = new System.Drawing.Point(401, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.아이콘저장ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(138, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 아이콘저장ToolStripMenuItem
            // 
            this.아이콘저장ToolStripMenuItem.Name = "아이콘저장ToolStripMenuItem";
            this.아이콘저장ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.아이콘저장ToolStripMenuItem.Text = "아이콘 저장";
            this.아이콘저장ToolStripMenuItem.Click += new System.EventHandler(this.아이콘저장ToolStripMenuItem_Click);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(348, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 12);
            this.label10.TabIndex = 38;
            this.label10.Text = "아이콘:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkWindowRefresh
            // 
            this.chkWindowRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkWindowRefresh.AutoSize = true;
            this.chkWindowRefresh.Checked = true;
            this.chkWindowRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWindowRefresh.Location = new System.Drawing.Point(508, 8);
            this.chkWindowRefresh.Name = "chkWindowRefresh";
            this.chkWindowRefresh.Size = new System.Drawing.Size(48, 16);
            this.chkWindowRefresh.TabIndex = 36;
            this.chkWindowRefresh.Text = "자동";
            this.chkWindowRefresh.UseVisualStyleBackColor = true;
            this.chkWindowRefresh.CheckedChanged += new System.EventHandler(this.chkWindowRefresh_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkWindowClose);
            this.groupBox5.Controls.Add(this.comboBox1);
            this.groupBox5.Controls.Add(this.chkAOT);
            this.groupBox5.Controls.Add(this.chkWindowEnable);
            this.groupBox5.Controls.Add(this.chkVisible);
            this.groupBox5.ForeColor = System.Drawing.Color.Blue;
            this.groupBox5.Location = new System.Drawing.Point(143, 128);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(160, 102);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "창 속성";
            // 
            // chkWindowClose
            // 
            this.chkWindowClose.AutoSize = true;
            this.chkWindowClose.Checked = true;
            this.chkWindowClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWindowClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkWindowClose.Location = new System.Drawing.Point(76, 39);
            this.chkWindowClose.Name = "chkWindowClose";
            this.chkWindowClose.Size = new System.Drawing.Size(76, 16);
            this.chkWindowClose.TabIndex = 33;
            this.chkWindowClose.Text = "닫기 버튼";
            this.chkWindowClose.UseVisualStyleBackColor = true;
            this.chkWindowClose.CheckedChanged += new System.EventHandler(this.chkWindowClose_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "타이틀기본",
            "타이틀없음",
            "타이틀고정",
            "타이틀얇게",
            "타이틀기본/고정",
            "타이틀얇게/고정"});
            this.comboBox1.Location = new System.Drawing.Point(6, 81);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(120, 20);
            this.comboBox1.TabIndex = 32;
            this.comboBox1.Visible = false;
            // 
            // chkAOT
            // 
            this.chkAOT.AutoSize = true;
            this.chkAOT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkAOT.Location = new System.Drawing.Point(5, 17);
            this.chkAOT.Name = "chkAOT";
            this.chkAOT.Size = new System.Drawing.Size(64, 16);
            this.chkAOT.TabIndex = 11;
            this.chkAOT.Text = "항상 위";
            this.chkAOT.UseVisualStyleBackColor = true;
            this.chkAOT.CheckedChanged += new System.EventHandler(this.chkAOT_CheckedChanged);
            // 
            // chkWindowEnable
            // 
            this.chkWindowEnable.AutoSize = true;
            this.chkWindowEnable.Checked = true;
            this.chkWindowEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWindowEnable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkWindowEnable.Location = new System.Drawing.Point(76, 17);
            this.chkWindowEnable.Name = "chkWindowEnable";
            this.chkWindowEnable.Size = new System.Drawing.Size(76, 16);
            this.chkWindowEnable.TabIndex = 22;
            this.chkWindowEnable.Text = "창 활성화";
            this.chkWindowEnable.UseVisualStyleBackColor = true;
            this.chkWindowEnable.CheckedChanged += new System.EventHandler(this.chkWindowEnable_CheckedChanged);
            // 
            // chkVisible
            // 
            this.chkVisible.AutoSize = true;
            this.chkVisible.Checked = true;
            this.chkVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkVisible.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkVisible.Location = new System.Drawing.Point(5, 39);
            this.chkVisible.Name = "chkVisible";
            this.chkVisible.Size = new System.Drawing.Size(64, 16);
            this.chkVisible.TabIndex = 31;
            this.chkVisible.Text = "창 보임";
            this.chkVisible.UseVisualStyleBackColor = true;
            this.chkVisible.CheckedChanged += new System.EventHandler(this.chkVisible_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.btnMinimize);
            this.groupBox4.Controls.Add(this.btnMaximize);
            this.groupBox4.Controls.Add(this.btnRestore);
            this.groupBox4.Controls.Add(this.btnClose);
            this.groupBox4.Controls.Add(this.btnBlink);
            this.groupBox4.Controls.Add(this.btnBTF);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Location = new System.Drawing.Point(2, 128);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(139, 102);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "창 작업";
            // 
            // btnMinimize
            // 
            this.btnMinimize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMinimize.Location = new System.Drawing.Point(3, 15);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(65, 23);
            this.btnMinimize.TabIndex = 12;
            this.btnMinimize.Text = "최소화";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnMaximize
            // 
            this.btnMaximize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMaximize.Location = new System.Drawing.Point(3, 39);
            this.btnMaximize.Name = "btnMaximize";
            this.btnMaximize.Size = new System.Drawing.Size(65, 23);
            this.btnMaximize.TabIndex = 13;
            this.btnMaximize.Text = "최대화";
            this.btnMaximize.UseVisualStyleBackColor = true;
            this.btnMaximize.Click += new System.EventHandler(this.btnMaximize_Click);
            // 
            // btnRestore
            // 
            this.btnRestore.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnRestore.Location = new System.Drawing.Point(3, 63);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(65, 23);
            this.btnRestore.TabIndex = 14;
            this.btnRestore.Text = "창 복원";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnClose
            // 
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClose.Location = new System.Drawing.Point(70, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(65, 23);
            this.btnClose.TabIndex = 20;
            this.btnClose.Text = "창 닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnBlink
            // 
            this.btnBlink.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBlink.Location = new System.Drawing.Point(70, 63);
            this.btnBlink.Name = "btnBlink";
            this.btnBlink.Size = new System.Drawing.Size(65, 23);
            this.btnBlink.TabIndex = 21;
            this.btnBlink.Text = "깜빡이기";
            this.btnBlink.UseVisualStyleBackColor = true;
            this.btnBlink.Visible = false;
            this.btnBlink.Click += new System.EventHandler(this.btnBlink_Click);
            // 
            // btnBTF
            // 
            this.btnBTF.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBTF.Location = new System.Drawing.Point(70, 39);
            this.btnBTF.Name = "btnBTF";
            this.btnBTF.Size = new System.Drawing.Size(65, 23);
            this.btnBTF.TabIndex = 23;
            this.btnBTF.Text = "앞으로";
            this.btnBTF.UseVisualStyleBackColor = true;
            this.btnBTF.Click += new System.EventHandler(this.btnBTF_Click);
            // 
            // txtClass
            // 
            this.txtClass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClass.BackColor = System.Drawing.SystemColors.Control;
            this.txtClass.Location = new System.Drawing.Point(51, 34);
            this.txtClass.Name = "txtClass";
            this.txtClass.ReadOnly = true;
            this.txtClass.Size = new System.Drawing.Size(511, 21);
            this.txtClass.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 33;
            this.label5.Text = "클래스";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numAbsY);
            this.groupBox3.Controls.Add(this.numAbsX);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.numRelY);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.numRelX);
            this.groupBox3.Controls.Add(this.numDesktopHeight);
            this.groupBox3.Controls.Add(this.numDesktopWidth);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.numClientHeight);
            this.groupBox3.Controls.Add(this.numClientWidth);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(305, 128);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 102);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "창 영역";
            // 
            // numAbsY
            // 
            this.numAbsY.Location = new System.Drawing.Point(68, 34);
            this.numAbsY.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numAbsY.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numAbsY.Name = "numAbsY";
            this.numAbsY.Size = new System.Drawing.Size(58, 21);
            this.numAbsY.TabIndex = 17;
            this.numAbsY.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // numAbsX
            // 
            this.numAbsX.Location = new System.Drawing.Point(7, 34);
            this.numAbsX.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numAbsX.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numAbsX.Name = "numAbsX";
            this.numAbsX.Size = new System.Drawing.Size(58, 21);
            this.numAbsX.TabIndex = 16;
            this.numAbsX.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(23, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "데스크탑 위치";
            // 
            // numRelY
            // 
            this.numRelY.Location = new System.Drawing.Point(68, 78);
            this.numRelY.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numRelY.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numRelY.Name = "numRelY";
            this.numRelY.Size = new System.Drawing.Size(58, 21);
            this.numRelY.TabIndex = 14;
            this.numRelY.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(149, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "데스크탑 크기";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(32, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "컨트롤 위치";
            // 
            // numRelX
            // 
            this.numRelX.Location = new System.Drawing.Point(7, 78);
            this.numRelX.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numRelX.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numRelX.Name = "numRelX";
            this.numRelX.Size = new System.Drawing.Size(58, 21);
            this.numRelX.TabIndex = 13;
            this.numRelX.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // numDesktopHeight
            // 
            this.numDesktopHeight.Location = new System.Drawing.Point(194, 34);
            this.numDesktopHeight.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numDesktopHeight.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numDesktopHeight.Name = "numDesktopHeight";
            this.numDesktopHeight.Size = new System.Drawing.Size(58, 21);
            this.numDesktopHeight.TabIndex = 11;
            this.numDesktopHeight.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // numDesktopWidth
            // 
            this.numDesktopWidth.Location = new System.Drawing.Point(133, 34);
            this.numDesktopWidth.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numDesktopWidth.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numDesktopWidth.Name = "numDesktopWidth";
            this.numDesktopWidth.Size = new System.Drawing.Size(58, 21);
            this.numDesktopWidth.TabIndex = 10;
            this.numDesktopWidth.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(156, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "컨트롤 크기";
            // 
            // numClientHeight
            // 
            this.numClientHeight.Location = new System.Drawing.Point(194, 78);
            this.numClientHeight.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numClientHeight.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numClientHeight.Name = "numClientHeight";
            this.numClientHeight.Size = new System.Drawing.Size(58, 21);
            this.numClientHeight.TabIndex = 8;
            this.numClientHeight.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // numClientWidth
            // 
            this.numClientWidth.Location = new System.Drawing.Point(133, 78);
            this.numClientWidth.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numClientWidth.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numClientWidth.Name = "numClientWidth";
            this.numClientWidth.Size = new System.Drawing.Size(58, 21);
            this.numClientWidth.TabIndex = 7;
            this.numClientWidth.ValueChanged += new System.EventHandler(this.Area_ValueChanged);
            // 
            // lblWindowTransparent
            // 
            this.lblWindowTransparent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWindowTransparent.AutoSize = true;
            this.lblWindowTransparent.Location = new System.Drawing.Point(532, 100);
            this.lblWindowTransparent.Name = "lblWindowTransparent";
            this.lblWindowTransparent.Size = new System.Drawing.Size(23, 12);
            this.lblWindowTransparent.TabIndex = 30;
            this.lblWindowTransparent.Text = "255";
            // 
            // btnWindowRefresh
            // 
            this.btnWindowRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWindowRefresh.Location = new System.Drawing.Point(436, 4);
            this.btnWindowRefresh.Name = "btnWindowRefresh";
            this.btnWindowRefresh.Size = new System.Drawing.Size(65, 23);
            this.btnWindowRefresh.TabIndex = 28;
            this.btnWindowRefresh.Text = "새로고침";
            this.btnWindowRefresh.UseVisualStyleBackColor = true;
            this.btnWindowRefresh.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblWindowHandle
            // 
            this.lblWindowHandle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblWindowHandle.ForeColor = System.Drawing.Color.Blue;
            this.lblWindowHandle.Location = new System.Drawing.Point(59, 9);
            this.lblWindowHandle.Name = "lblWindowHandle";
            this.lblWindowHandle.ReadOnly = true;
            this.lblWindowHandle.Size = new System.Drawing.Size(141, 14);
            this.lblWindowHandle.TabIndex = 27;
            this.lblWindowHandle.Text = "0x00000000 (000000)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(6, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 12);
            this.label4.TabIndex = 26;
            this.label4.Text = "창 핸들:";
            // 
            // tbWindowTransparent
            // 
            this.tbWindowTransparent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWindowTransparent.AutoSize = false;
            this.tbWindowTransparent.Location = new System.Drawing.Point(51, 88);
            this.tbWindowTransparent.Maximum = 255;
            this.tbWindowTransparent.Minimum = 1;
            this.tbWindowTransparent.Name = "tbWindowTransparent";
            this.tbWindowTransparent.Size = new System.Drawing.Size(472, 36);
            this.tbWindowTransparent.TabIndex = 19;
            this.tbWindowTransparent.TickFrequency = 5;
            this.tbWindowTransparent.Value = 1;
            this.tbWindowTransparent.ValueChanged += new System.EventHandler(this.tbWindowTransparent_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 18;
            this.label2.Text = "투명도";
            // 
            // btnWindowTitle
            // 
            this.btnWindowTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWindowTitle.Location = new System.Drawing.Point(477, 62);
            this.btnWindowTitle.Name = "btnWindowTitle";
            this.btnWindowTitle.Size = new System.Drawing.Size(65, 21);
            this.btnWindowTitle.TabIndex = 17;
            this.btnWindowTitle.Text = "바꾸기";
            this.btnWindowTitle.UseVisualStyleBackColor = true;
            this.btnWindowTitle.Click += new System.EventHandler(this.btnWindowTitle_Click);
            // 
            // txtWindowTitle
            // 
            this.txtWindowTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWindowTitle.Location = new System.Drawing.Point(51, 62);
            this.txtWindowTitle.Name = "txtWindowTitle";
            this.txtWindowTitle.Size = new System.Drawing.Size(422, 21);
            this.txtWindowTitle.TabIndex = 16;
            this.txtWindowTitle.TextChanged += new System.EventHandler(this.txtWindowTitle_TextChanged);
            this.txtWindowTitle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWindowTitle_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "타이틀";
            // 
            // btnWindowsRefresh
            // 
            this.btnWindowsRefresh.Location = new System.Drawing.Point(221, 16);
            this.btnWindowsRefresh.Name = "btnWindowsRefresh";
            this.btnWindowsRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnWindowsRefresh.TabIndex = 14;
            this.btnWindowsRefresh.Text = "잡기";
            this.btnWindowsRefresh.UseVisualStyleBackColor = true;
            this.btnWindowsRefresh.Click += new System.EventHandler(this.btnWindowsRefresh_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(4, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "핸들";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(574, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.끝내기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 끝내기XToolStripMenuItem
            // 
            this.끝내기XToolStripMenuItem.Name = "끝내기XToolStripMenuItem";
            this.끝내기XToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.끝내기XToolStripMenuItem.Text = "끝내기(&X)";
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로고정ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 맨위로고정ToolStripMenuItem
            // 
            this.맨위로고정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로고정ToolStripMenuItem.Name = "맨위로고정ToolStripMenuItem";
            this.맨위로고정ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.맨위로고정ToolStripMenuItem.Text = "맨 위로 고정";
            this.맨위로고정ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위로고정ToolStripMenuItem_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // btnProcessList
            // 
            this.btnProcessList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProcessList.Location = new System.Drawing.Point(0, 309);
            this.btnProcessList.Name = "btnProcessList";
            this.btnProcessList.Size = new System.Drawing.Size(287, 23);
            this.btnProcessList.TabIndex = 3;
            this.btnProcessList.Text = "프로세스 목록";
            this.btnProcessList.UseVisualStyleBackColor = true;
            this.btnProcessList.Click += new System.EventHandler(this.btnProcessList_Click);
            // 
            // btnWindowList
            // 
            this.btnWindowList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWindowList.Enabled = false;
            this.btnWindowList.Location = new System.Drawing.Point(288, 309);
            this.btnWindowList.Name = "btnWindowList";
            this.btnWindowList.Size = new System.Drawing.Size(285, 23);
            this.btnWindowList.TabIndex = 4;
            this.btnWindowList.Text = "창 목록";
            this.btnWindowList.UseVisualStyleBackColor = true;
            this.btnWindowList.Click += new System.EventHandler(this.btnWindowList_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.ico";
            this.saveFileDialog1.Filter = "아이콘 파일|*.ico|PNG 파일|*.png";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 332);
            this.Controls.Add(this.btnWindowList);
            this.Controls.Add(this.btnProcessList);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(590, 370);
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.Text = "HS™ 창 관리자";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.frmMain_SizeChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWindowHandle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAbsY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAbsX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRelY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRelX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDesktopHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDesktopWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numClientHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numClientWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWindowTransparent)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnWindowsRefresh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBlink;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TrackBar tbWindowTransparent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnWindowTitle;
        private System.Windows.Forms.TextBox txtWindowTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Button btnMaximize;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.CheckBox chkAOT;
        private System.Windows.Forms.CheckBox chkWindowEnable;
        private System.Windows.Forms.Button btnBTF;
        private System.Windows.Forms.NumericUpDown numWindowHandle;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lblWindowHandle;
        private System.Windows.Forms.Button btnWindowRefresh;
        private System.Windows.Forms.Label lblWindowTransparent;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox chkVisible;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnFindWindow;
        private Spy.SpyControl spyControl1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numDesktopHeight;
        private System.Windows.Forms.NumericUpDown numDesktopWidth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numClientHeight;
        private System.Windows.Forms.NumericUpDown numClientWidth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numRelY;
        private System.Windows.Forms.NumericUpDown numRelX;
        private System.Windows.Forms.NumericUpDown numAbsY;
        private System.Windows.Forms.NumericUpDown numAbsX;
        private System.Windows.Forms.CheckBox chkWindowRefresh;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 끝내기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem 맨위로고정ToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox chkWindowClose;
        private System.Windows.Forms.Button btnProcessList;
        private System.Windows.Forms.Button btnWindowList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 아이콘저장ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button btnProcessGoTo;
    }
}

