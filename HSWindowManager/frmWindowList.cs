﻿using HSWindowManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HSWindowManager
{
    public partial class frmWindowList : Form
    {
        Process process;
        ProcessThread thread;
        Process parent;
        public frmWindowList(Process process)
        {
            this.process = process;
            InitializeComponent();
        }
        public frmWindowList(ProcessThread thread, Process parent)
        {
            this.thread = thread;
            this.parent = parent;
            InitializeComponent();
        }
        public frmWindowList(ProcessThread thread, Process parent, List<WindowItemEx> Init)
        {
            this.thread = thread;
            this.parent = parent;
            InitializeComponent();

            listView1.Items.Clear();
            ListViewItem[] li = new ListViewItem[Init.Count];
            for (int i = 0; i < Init.Count; i++)
            {
                li[i] = new ListViewItem("0x" + Init[i].Handle.ToString("X8"));
                li[i].SubItems.Add(Init[i].ClassName + (Init[i].RealClassName == Init[i].ClassName ? "" : " (" + Init[i].RealClassName + ")"));
                li[i].SubItems.Add(Init[i].Text);
                li[i].Tag = Init[i];
            }
            listView1.Items.AddRange(li);

            listView1.EndUpdate();
        }

        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
         
        private void 새로고침RToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (process == null)
            {
                WindowItemEx sel = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0].Tag as WindowItemEx : null;

                int scrollSpot = listView1.TopItem == null ? 0 : listView1.TopItem.Index;
                //WindowAPIEx.LockWindowUpdate(listView1.Handle);
                //int horzScroll = WindowAPIEx.GetScrollPos(listView1.Handle, WindowAPIEx.SBS_VERT);

                //listView1.BeginUpdate();

                listView1.Items.Clear();
                List<WindowItemEx> list = WindowItemEx.GetThreadWindows((uint)thread.Id);
                ListViewItem[] li = new ListViewItem[list.Count];
                int select = -1;
                for (int i = 0; i < list.Count; i++)
                {
                    li[i] = new ListViewItem("0x" + list[i].Handle.ToString("X8"));
                    li[i].SubItems.Add(list[i].ClassName + (list[i].RealClassName == list[i].ClassName ? "" : " (" + list[i].RealClassName + ")"));
                    li[i].SubItems.Add(list[i].Text);
                    li[i].Tag = list[i];
                    if (sel != null && list[i] == sel) select = i;
                }

                listView1.Items.AddRange(li);
                if (select > -1 && select < listView1.Items.Count)
                {
                    listView1.EnsureVisible(select);
                    listView1.Items[select].Focused = true;
                    listView1.Items[select].Selected = true;
                }

                //listView1.EndUpdate();

                WindowAPIEx.SendMessage(listView1.Handle, WindowAPIEx.LVM_SCROLL, (IntPtr)scrollSpot, IntPtr.Zero);
                //WindowAPIEx.LockWindowUpdate(IntPtr.Zero);
            }
            else
            {
                
            }
        }

        private void WindowList_Load(object sender, EventArgs e)
        {
            this.Text = process == null ? "창 목록 (TID: " + thread.Id + ") ["+ parent.ProcessName+"]" : "창 목록 (PID: " + process.Id + ")";

            if(listView1.Items.Count < 1)새로고침RToolStripMenuItem_Click(this, e);
            toolStripComboBox1.SelectedIndex = 0;
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                WindowItemEx ex = (WindowItemEx)listView1.SelectedItems[0].Tag;
                if (!ex.Exists) MessageBox.Show(this.Text, "종료된 창입니다.\n\n새로고침을 해주시기 바랍니다.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (frmMain.Instance != null) { frmMain.Instance.getWindow(ex); frmMain.Instance.BringToFront(); }
            }
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) listView1_MouseDoubleClick(this, null);
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 1: timer1.Interval = 2000; timer1.Start(); break;
                case 2: timer1.Interval = 1000; timer1.Start(); break;
                case 3: timer1.Interval = 500; timer1.Start(); break;
                case 4: timer1.Interval = 250; timer1.Start(); break;
                default: timer1.Stop(); break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            새로고침RToolStripMenuItem_Click(this, e);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            //
        }

        private void 맨위로고정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위로고정ToolStripMenuItem.Checked;
        }
    }
}
